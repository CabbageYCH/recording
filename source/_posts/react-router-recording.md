---
title: React-Router相关内容整理
date: 2021-04-12 10:25:06
tags: [前端, React-Router, 官方文档整理]
categories: React-Router
---
 &emsp; &emsp; 根据[官方文档](https://reactrouter.com/web/guides/quick-start)，对React-Router内容进行系统性梳理。

## React中常用的对象

### history

&emsp; &emsp; `history` 对象有如下属性和方法：

* length - (number) The number of entries in the history stack
* action - (string) The current action (PUSH, REPLACE, or POP)
* location - (object) The current location. May have the following properties:
* pathname - (string) The path of the URL
* search - (string) The URL query string
* hash - (string) The URL hash fragment
* state - (object) location-specific state that was provided to e.g. push(path, state) when this location was pushed onto the stack. Only available in browser and memory history.
* push(path, [state]) - (function) Pushes a new entry onto the history stack
* replace(path, [state]) - (function) Replaces the current entry on the history stack
* go(n) - (function) Moves the pointer in the history stack by n entries
* goBack() - (function) Equivalent to go(-1)
* goForward() - (function) Equivalent to go(1)
* block(prompt) - (function) Prevents navigation (see the history docs)

### location

&emsp; &emsp; `location` 代表该应用程序当前的位置，希望其运行的位置，甚至是它之前所在的位置。形如：

``` jsx
{
  key: 'ac3df4', // not with HashHistory!
  pathname: '/somewhere',
  search: '?some=search-string',
  hash: '#howdy',
  state: {
    [userDefined]: true
  }
}
```

#### 可以在如下位置访问到 `location`

* `Route component` as this.props.location
* `Route render` as ({ location }) => ()
* `Route children` as ({ location }) => ()
* `withRouter` as this.props.location

### match

&emsp; &emsp; `match` 对象表示 `Route` 的路由匹配情况，其属性如下：

* params - (object) Key/value pairs parsed from the URL corresponding to the dynamic segments of the path
* isExact - (boolean) true if the entire URL was matched (no trailing characters)
* path - (string) The path pattern used to match. Useful for building nested `<Route>`s
* url - (string) The matched portion of the URL. Useful for building nested `<Link>`s

#### 如下地方可以访问到 `match`

* `Route component` as this.props.match
* `Route render` as ({ match }) => ()
* `Route children` as ({ match }) => ()
* `withRouter` as this.props.match
* `matchPath` as the return value
* `useRouteMatch` as the return value

#### `match` 为 `null` 的情况

&emsp; &emsp; `Route` 使用 `children` 属性进行组件渲染，且 `path` 与当前路由不匹配

## Guides

### code splitting

&emsp; &emsp; `react-loadable` 是用于通过动态导入加载组件的库。它可以自动处理各种边缘情况，并且使代码拆分变得简单！代码示例：

``` jsx
import Loadable from 'react-loadable';
import Loading from './Loading';

const LoadableComponent = Loadable({
  loader: () => import('./Dashboard'),
  loading: Loading,
})

export default class LoadableDashboard extends React.Component {
  render() {
    return <LoadableComponent />;
  }
}
```

&emsp; &emsp; `loader` 是一个实际加载组件的函数，而 `loading` 是在实际组件加载时显示的占位符组件。

--------

## API

### Hooks

#### useHistory

&emsp; &emsp; `useHistory` 用于获取可用于导航的 `history` 实例。

``` jsx
import { useHistory } from "react-router-dom";

const HomeButton = () => {
    let history = useHistory();

    // 点击Go home按钮，页面路由切换为/home
    function handleClick() {
        history.push("/home");
    }

    return (
        <button type="button" onClick={handleClick}>
            Go home
        </button>
    );
}

export default HomeButton;
```

#### useLocation

&emsp; &emsp; `useLocation` 返回表示当前URL的对象。其表现类似于 `useState` ，它会在URL更改时返回一个新的值。

``` jsx
import { useEffect } from "react";
import { useLocation } from "react-router-dom";

const LocationChange = () => {
    let location = useLocation();

    useEffect(() => {
        console.log('location:', location);
    }, [location])

    return null;
}

export default LocationChange;
```

运行截图如下：
{% asset_img use-location.png %}

#### useParams

&emsp; &emsp; `useParams` 用于获取路由中的params。

``` jsx
import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    useParams,
    Link
} from "react-router-dom";
import Home from '../../home';

const GetParams = () => {
    function BlogPost() {
        console.log(useParams()); // log: {slug: "params"}
        let { slug } = useParams() as any;
        return <div>Now showing post {slug}</div>;
    }

    return (
        <React.Fragment>
            <Router>
                <Link to='/'>home</Link>
                <Link to='/blog/params'>blogPost </Link>
                <Switch>
                    <Route path='/blog/:slug'>
                        <BlogPost />
                    </Route>
                    <Route path='/'>
                        <Home />
                    </Route>
                </Switch>
            </Router>
        </React.Fragment>
    )
}

export default GetParams;

```

#### useRouterMatch

&emsp; &emsp; `useRouterMatch` 用于匹配当前路由

``` jsx
import { useRouteMatch } from "react-router-dom";

const BlogPost = () => {
    const match = useRouteMatch("/routerMatch/:slug"); // 参数形式一
    // 参数形式二
    const match2 = useRouteMatch({
        path: "/routermatch/:slug/",
        strict: true,
        sensitive: true
    });

    console.log(match);
    //log: {isExact: true, params: {slug: "params"}, path: "/routerMatch/:slug", url: "/routerMatch/params"}

    console.log(match2);
    //log: null
    // path: "/routermatch/:slug/"  结尾多了‘/’，大小写没匹配
    if (match) {
        return <div>match</div>;
    } else {
        return null;
    }
}

const RouterMatch = () => {
    return (
        <Router>
            <Link to='/'>home</Link>
            <Link to='/routerMatch/params'>blogPost </Link>
            <Switch>
                <Route path='/routerMatch/:slug'><BlogPost /></Route>
                <Route path='/'><BlogPost /></Route>
            </Switch>
        </Router>
    );
}
```

--------

### BrowserRouter

&emsp; &emsp; 利用 `HTML5` 的 `history` API( `pushState, replaceState, popstate` )实现导航。

``` jsx
import { BrowserRouter as Router } from 'react-router-dom';

const BrowserRouterApp = () => {
    return (
        <Router
            basename={'base-name'} // 所有路由的基础地址
            forceRefresh={false} // 是否在路由改变时刷新整个页面
            getUserConfirmation={() => window.confirm('是否跳转？')} 
            // 确认是否跳转，默认使用window.confirm（需要配合prompt使用）
            keyLength={12} // location.key长度，默认6
        >
            <Link to='/home'>home</Link>
            <br />
            <Link to='/about'>about</Link>
            <Switch>
                <Route path='/about'>
                    <About />
                </Route>
                <Route path='/home'>
                    <Home />
                </Route>
            </Switch>
        </Router>
    );
}
```

### HashRouter

&emsp; &emsp; 使用 `URL` 中的哈希部分进行导航。

``` jsx
const HashRouterApp = () => {
    return (
        <Router
            basename={'hash-router'}
            getUserConfirmation={() => window.confirm('确定跳转？')}
            hashType={'hashbang'}
        >
            <Link to='/home'>home</Link> 
            {/* http://localhost:3000/#!/hash-router/home */}
            <br />
            <Link to='/about'>about</Link>
            {/* http://localhost:3000/#!/hash-router/about */}
            <Switch>
                <Route path='/about'>
                    <About />
                </Route>
                <Route path='/home'>
                    <Home />
                </Route>
            </Switch>
        </Router>
    );
}
```

`hashType` 有三个可选值(默认为 `slash` )：

* "slash" - 产生的哈希部分形如 `#/` 以及 `#/sunshine/lollipops`
* "noslash" - 产生的哈希部分形如 `#` 以及 `#sunshine/lollipops`
* "hashbang" - 产生的哈希部分形如 `#!/` 以及 `#!/sunshine/lollipops`

### Link

&emsp; &emsp; 点击前往对应页面，功能相当于 `a` 标签。

#### to

&emsp; &emsp; `to` 前往要跳转的页面，参数类型有：

* `string`
* `object`
* `function`, 传入参数为当前`location`，返回值为`string`或者是`object`

#### replace: bool

&emsp; &emsp; `replace` 为 `true` 时，点击链接，将会替换 `history` 栈中的当前条目, 而不是在栈中新增一条记录

#### component

&emsp; &emsp; 如果想要复用已有的导航组件，可将 `component` 设置为自己的导航组件
&emsp; &emsp; 代码示例如下：

``` jsx
const LinkApp = () => {
    return (
        <Router
            basename={'hash-router'}
            hashType={'hashbang'}
        >
            <Link
                to={{
                    pathname: "/home",
                    search: "?arg=arg1",
                    hash: "#the-hash",
                    state: { fromDashboard: true }
                }}
                replace
            >
                Home
            </Link>

            <br />
            <Link to={location => ({ ...location, pathname: "/about" })} > About </Link>
            <Switch>
                <Route path='/about'>
                    <About />
                </Route>
                <Route path='/home'>
                    <Home />
                </Route>
            </Switch>
        </Router>
    );
}
```

### NavLink 

&emsp; &emsp; 加强版 `<Link />` ，当 `to` 要前往的页面与当前URL匹配时，该标签对应的特定样式生效。

#### activeClassName: string

&emsp; &emsp; 对应标签的 `className`

#### activeStyle: object

&emsp; &emsp; 对应行内样式

#### exact: bool

&emsp; &emsp; 只有路由相同时，设置的样式才会生效

#### strict: bool

&emsp; &emsp; 在进行路由匹配时，不可以略过路由中的 `/` 。

#### isActive: func

&emsp; &emsp; 添加额外的逻辑判断当前的链接是否处于 `active` 状态

``` jsx
  isActive={(match, location) => {
    // ...
  }}
```

### Prompt

&emsp; &emsp; 在离开页面前，对用户进行提示。

#### message: string | func

* string

&emsp; &emsp; 离开当前页面的提示信息

* func

&emsp; &emsp; 参数对应跳往页面的 `location` 和 `action` ，返回值为 `true` 则允许跳转，返回值为 `string` 类型则为用户离开页面前的提示信息。

``` jsx
<Prompt
  message={(location, action) => {
    if (action === 'POP') {
      console.log("Backing up...")
    }

    return location.pathname.startsWith("/app")
      ? true
      : `Are you sure you want to go to ${location.pathname}?`
  }}
/>

```

#### when: bool

&emsp; &emsp; 通过传入 `true or false` 来控制是否渲染 `<Prompt />`

### MemoryRouter

&emsp; &emsp; 将 `URL` 变动历史存储在内存中（不直接对地址栏进行操作），常用于测试和非浏览器环境。

### Redirect

&emsp; &emsp; 导航至一个新的页面（ `history stack` 中对应的当前页面的 `location` 会被覆盖）。

#### to: string | object

* string

&emsp; &emsp; 略

* object

&emsp; &emsp; 形如：

``` jsx
<Redirect
  to={{
    pathname: "/login",
    search: "?utm=your+face",
    state: { referrer: currentLocation }
  }}
/>

```

#### push: bool

&emsp; &emsp; 当值为 `true` 的时候，重定向组件会往 `history stack` 中增加一个新的 `location` ，而不是替换当前 `location` 。

#### from: string

&emsp; &emsp; `from` 表示跳转前的页面地址。只有当前地址与 `from` 的地址匹配时，页面跳转才会生效。

> 此属性需要将 `<Redirect>` 放置在 `<Switch>` 内才会生效。

#### exact、strict、sensitive: bool

&emsp; &emsp; 三者都只有在 `from` ， `to` 一起用，且 `<Redirect>` 放置在 `<Switch>` 内的情况下才会生效。

### Route

&emsp; &emsp; `React Router` 中最基础的组件，用于渲染 `path` 对应的路由组件。

#### render methods

&emsp; &emsp; `Route` 用于渲染组件的方法有（建议使用子元素的形式进行组件渲染）：

* `Route component`

&emsp; &emsp; 当使用 `component` 渲染组件时， `React` 会使用 `React.createElement` 创建一个新的组件。这意味着，如果把内联函数赋值给 `component` ，则将在每次渲染中创建一个新组件。这将导致现有组件的卸载和新组件的安装，而不是更新现有组件。当使用内联函数进行内联渲染时，请使用 `render` 或 `children` 。

``` jsx
ReactDOM.render(
  <Router>
    <Route path="/user/:username" component={User} />
  </Router>,
  node
);
```

* `Route render`

``` jsx
ReactDOM.render(
  <Router>
    <Route path="/home" render={() => <div>Home</div>} />
  </Router>,
  node
);
```

> `component` 的优先级比 `render` 高，所以不要同时使用这两个属性。

* `Route children`

&emsp; &emsp; 当 `Route` 没有 `path` 属性时， `children` 对应的组件默认会被渲染

> `children` 的优先级高于 `render` 与 `component`

#### Route props

&emsp; &emsp; `Route` 具有如下属性：

* match
* location
* history

#### path: string | string[]

&emsp; &emsp; `Route` 在没有 `path` 属性的情况下，默认匹配。

``` jsx
<Route path={["/users/:id", "/profile/:id"]}>
  <User />
</Route>
```

#### location: object

&emsp; &emsp; 当 `Route` 需要对不同于当前页面的URL进行匹配时，可以将 `location` 赋值给 `Switch` ，再将 `Route` 组件包裹在 `Switch` 中即可。

``` jsx
<Switch location={location}>
    <Route path="/hsl/:h/:s/:l" children={<HSL />} />
    <Route path="/rgb/:r/:g/:b" children={<RGB />} />
</Switch>
```

#### exact、sensitive、strict: bool

&emsp; &emsp; 用于 `path` 匹配。

### Router

&emsp; &emsp; `Router` 是所有路由组件的通用底层接口。

#### history: object

&emsp; &emsp; 用于导航的 `history` 。

### StaticRouter

&emsp; &emsp; `location` 不变的 `Router` ，常用语服务端渲染。

### Switch

&emsp; &emsp; 渲染子组件中第一个匹配的 `Route` ( `Redirect` )，即使有多个匹配的组件也只会渲染第一个。

#### location: object

&emsp; &emsp; `Switch` 中的 `location` 会覆盖子元素上的 `location` 。

### matchPath

&emsp; &emsp; 使用与 `Route` 一样的路由匹配代码，使用方式如下：

``` jsx
const match = matchPath("/users/123", {
  path: "/users/:id",
  exact: true,
  strict: false
});
```

&emsp; &emsp; 当路由匹配成功时，返回结果如下：

``` jsx
{
    isExact: true
    params: {
        id: "2"
    }
    path: "/users/:id"
    url: "/users/2"
}
```

&emsp; &emsp; 当路由匹配失败时，返回 `null` 。

### withRouter

&emsp; &emsp; 默认情况下必须是经过路由匹配渲染的组件才存在路由参数，才能使用编程式导航的写法(执行 `this.props.history.push('/detail')` 等语句)。然而不是所有组件都直接与路由相连（通过路由跳转到此组件）的，当这些组件需要路由参数时，使用 `withRouter` 就可以给此组件传入路由参数。

``` jsx
// A simple component that shows the pathname of the current location
class ShowTheLocation extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  render() {
    const { match, location, history } = this.props;

    return <div>You are now at {location.pathname}</div>;
  }
}

// Create a new component that is "connected" (to borrow redux
// terminology) to the router.
const ShowTheLocationWithRouter = withRouter(ShowTheLocation);
```
