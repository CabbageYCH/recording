---
title: react相关内容整理
date: 2021-01-13 00:01:55
tags: [前端, React, 官方文档整理]
categories: 框架相关
---
&emsp; &emsp; 重新阅读官方文档，对React相关内容进行梳理，整理一些容易忽略的点，文章按照文档顺序进行整理。

### 核心概念

* #### State & 生命周期

##### State更新可能异步

&emsp; &emsp; 出于性能考虑， `React` 可能会把多个 `setState()` 调用合并成一个调用。因为 `this.props` 和 `this.state` 可能会异步更新，所以你不要依赖他们的值来更新下一个状态。要解决这个问题，可以让 `setState()` 接收一个函数而不是一个对象。这个函数用上一个 state 作为第一个参数，将此次更新被应用时的 props 做为第二个参数：

``` javascript
this.setState((state, props) => ({
    counter: state.counter + props.increment
}));
```

---------

* #### 事件处理

##### 在React中不能通过返回false的方式阻止默认行为。必须显式的使用preventDefault

&emsp; &emsp; 例如，传统的 `HTML` 中阻止链接默认打开一个新页面，你可以这样写：

``` html
<a href="#" onclick="console.log('The link was clicked.'); return false">
    Click me
</a>
```

在 React 中，可能是这样的：

``` jsx
function ActionLink() {
  function handleClick(e) {
    e.preventDefault();
    console.log('The link was clicked.');
  }

  return (
    <a href="#" onClick={handleClick}>
      Click me
    </a>
  ); 
}
```

在这里， `e` 是一个合成事件。 `React` 根据 `W3C` 规范来定义这些合成事件，所以你不需要担心跨浏览器的兼容性问题。 `React` 事件与原生事件不完全相同。如果想了解更多，请查看 `SyntheticEvent` 参考指南。

##### React中，class的方法默认不会绑定this

&emsp; &emsp; 你必须谨慎对待 `JSX` 回调函数中的 `this` ，在 `JavaScript` 中， `class` 的方法默认不会绑定 `this` 。如果你忘记绑定 `this.handleClick` 并把它传入了 `onClick` ，当你调用这个函数的时候 `this` 的值为 `undefined` 。

--------

* #### 条件渲染

##### 阻止组件渲染

&emsp; &emsp; 在 `render` 方法中返回 `null` ，可以阻止对组件的渲染. 同时，该操作并不会影响组件的生命周期。

--------

* #### 列表 & Key

##### 使用key需要注意的地方（详细可参考[协调](https://zh-hans.reactjs.org/docs/reconciliation.html)）

> &emsp; 如果列表项目的顺序可能会变化，不建议使用索引来用作 `key` 值，因为这样做会导致性能变差，还可能引起组件状态的问题。如果选择不指定显式的 `key` 值，那么 `React` 将默认使用索引用作为列表项目的 `key` 值。
> &emsp; `key` 在其兄弟节点之间应该是独一无二的。然而，它们不需要是全局唯一的。

&emsp; &emsp; 错误使用 `key` 的代码如下：

``` jsx
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      arr: ['data0', 'data1', 'data2', 'data3']
    }
  }
  render() {
    return (
      <>
        <p>arr:  {this.state.arr.join(',')}</p>
        {this.state.arr.map(
          (el, index) =>
            <p key={index}>
              index:{index}&emsp;&emsp;<input />
            </p>
        )}
        <button
          onClick={() => {
            const arr = [...this.state.arr];
            arr.splice(1, 1);
            this.setState(preState => ({
              arr: arr
            }));
          }}
        >
          delete
        </button>
      </>
    );
  }
}
```

运行结果截图如下:
{% asset_img key-1.png %}
点击删除按钮后表现如下:
{% asset_img key-2.png %}&emsp; &emsp; 点击 `delete` 按钮，删除数组中的第二个元素，预期效果应该是对应内容为 `1` 的 `input` 框被删除，但实际却是最后一个 `input` 框被删除。
&emsp; &emsp; 这是因为 `React` 的 `diff` 算法在对比更新前后的两棵 `React` 树时，对于同一节点（记为A节点）的子节点，每个子节点都有一个唯一（兄弟节点间唯一）的 `key` 值来标记身份， A节点下不相等的 `key` 的数量的增减会被认为是子节点的增加和删除（即 `React` 在将改动渲染到页面上时，直接插入或是删除 `Dom` ）。

> 对于兄弟节点间 `key` 值重复的情况，根据 `React` 版本不同，可能会有不同的表现。

##### `React` 使用 `key` 的原因（详细可参考[协调](https://zh-hans.reactjs.org/docs/reconciliation.html)）

&emsp; &emsp; 在默认条件下，当递归 `DOM` 节点的子元素时， `React` 会同时遍历两个子元素的列表；当产生差异时，生成一个 `mutation` 。
&emsp; &emsp; 在子元素列表末尾新增元素时，更新开销比较小。比如：

``` html
<ul>
    <li>first</li>
    <li>second</li>
</ul>

<ul>
    <li>first</li>
    <li>second</li>
    <li>third</li>
</ul>
```

`React` 会先匹配两个 `<li>first</li>` 对应的树，然后匹配第二个元素 `<li>second</li>` 对应的树，最后插入第三个元素的 `<li>third</li>` 树。
&emsp; &emsp; 而如果只是简单的将新增元素插入到表头，那么更新开销会比较大。比如：

``` html
<ul>
    <li>Duke</li>
    <li>Villanova</li>
</ul>

<ul>
    <li>Connecticut</li>
    <li>Duke</li>
    <li>Villanova</li>
</ul>
```

`React` 不会意识到应该保留 `<li>Duke</li>` 和 `<li>Villanova</li>` ，而是会重建每一个子元素 。这种情况会带来性能问题。

--------

* #### 表单

##### 受控组件

&emsp; &emsp; 在 `HTML` 中，表单元素（如 `<input>` 、 `<textarea>` 和 `<select>` ）通常自己维护 `state` ，并根据用户输入进行更新。而在 `React` 中，可变状态（ `mutable state` ）通常保存在组件的 `state` 属性中，并且只能通过使用 `setState()` 来更新。
&emsp; &emsp; 把两者结合起来，使 `React` 的 `state` 成为“唯一数据源”。渲染表单的 `React` 组件还控制着用户输入过程中表单发生的操作。被 `React` 以这种方式控制取值的表单输入元素就叫做“受控组件”。

##### 受控组件输入空值

&emsp; &emsp; 在受控组件上指定 `value` 的值会阻止用户更改输入。如果你指定了 `value` ，但输入仍可编辑，则可能是你意外地将 `value` 设置为 `undefined` 或 `null` 。
<hr>

### 高级指引

* #### 代码分割

##### React.lazy

&emsp; &emsp; `React.lazy` 函数能让你像渲染常规组件一样处理动态引入（的组件）。
使用之前：

``` javascript
import OtherComponent from './OtherComponent';
```

使用之后：

``` javascript
const OtherComponent = React.lazy(() => import('./OtherComponent'));
```

此代码将会在组件首次渲染时，自动导入包含 `OtherComponent` 组件的包。
&emsp; &emsp; `React.lazy` 接受一个函数，这个函数需要动态调用 `import()` 。它必须返回一个 `Promise` ，该 `Promise` 需要 `resolve` 一个 `default export` 的 `React` 组件。
&emsp; &emsp; 然后应在 `Suspense` 组件中渲染 `lazy` 组件，如此使得我们可以使用在等待加载 `lazy` 组件（一个 `Suspense` 组件可以包裹多个 `lazy` 组件）时做优雅降级（如 `loading` 指示器等）。代码实例如下：

``` jsx
import React, { Suspense } from 'react';
const OtherComponent = React.lazy(() => import('./OtherComponent'));

function MyComponent() {
  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <OtherComponent />
      </Suspense>
    </div>
  );
}
```

##### 异常捕获边界

&emsp; &emsp; 如果模块加载失败（如网络问题），它会触发一个错误。你可以通过[异常捕获边界（ `Error boundaries` ）](https://zh-hans.reactjs.org/docs/error-boundaries.html)技术来处理这些情况，以显示良好的用户体验并管理恢复事宜。具体实现可参考下文[错误边界](#错误边界)中的代码实例。

##### 命名导出

&emsp; &emsp; `React.lazy` 目前只支持默认导出（ `default exports` ）。如果想被引入的模块使用命名导出，可以创建一个中间模块，来重新导出为默认模块。这能保证 `tree shaking` 不会出错，并且不必引入不需要的组件。代码实例如下:

``` js
// ManyComponents.js
export const MyComponent = /* ... */ ;
export const MyUnusedComponent = /* ... */ ;

// MyComponent.js
export {
    MyComponent as
    default
}
from "./ManyComponents.js";

// MyApp.js
import React, {
    lazy
} from 'react';
const MyComponent = lazy(() => import("./MyComponent.js"));
```

--------

* #### Context

##### 注意事项

&emsp; &emsp; 因为 `context` 会使用参考标识 `（reference identity）` 来决定何时进行渲染，这里可能会有一些陷阱，当 `provider` 的父组件进行重渲染时，可能会在 `consumers` 组件中触发意外的渲染。举个例子，当每一次 `Provider` 重渲染时，以下的代码会重渲染所有下面的 `consumers` 组件，因为 `value` 属性总是被赋值为新的对象：

``` jsx
class App extends React.Component {
  render() {
    return (
      <MyContext.Provider value={{something: 'something'}}>
        <Toolbar />
      </MyContext.Provider>
    );
  }
}
```

为了防止这种情况，将 `value` 状态提升到父节点的 `state` 里：

``` jsx
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: {something: 'something'},
    };
  }

  render() {
    return (
      <Provider value={this.state.value}>
        <Toolbar />
      </Provider>
    );
  }
}
```

--------

* #### 错误边界

&emsp; &emsp; 部分 `UI` 的 `JavaScript` 错误不应该导致整个应用崩溃，为了解决这个问题， `React 16` 引入了一个新的概念 —— 错误边界。
&emsp; &emsp; 错误边界是一种 `React` 组件，这种组件可以捕获并打印发生在其子组件树任何位置的 `JavaScript` 错误，并且，它会渲染出备用 `UI` ，而不是渲染那些崩溃了的子组件树。错误边界在渲染期间、生命周期方法和整个组件树的构造函数中捕获错误。
&emsp; &emsp; 如果一个 `class` 组件中定义了 `static getDerivedStateFromError()` 或 `componentDidCatch()` 这两个生命周期方法中的任意一个（或两个）时，那么它就变成一个错误边界。当抛出错误后，请使用 `static getDerivedStateFromError()` 渲染备用 `UI` ，使用 `componentDidCatch()` 打印错误信息。将错误边界和上文提到的[代码分割](#代码分割)结合，代码实例如下：

``` jsx
// MyComponent.jsx
import React, { Suspense } from 'react';
import MyErrorBoundary from './MyErrorBoundary';

const OtherComponent = React.lazy(() => import('./OtherComponent'));

const MyComponent = () => (
  <div>
    <MyErrorBoundary>
      <Suspense fallback={<div>Loading...</div>}>
        <section>
          <OtherComponent />
        </section>
      </Suspense>
    </MyErrorBoundary>
  </div>
);

// MyErrorBoundary.jsx
class MyErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // 更新 state 使下一次渲染能够显示降级后的 UI
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // 你同样可以将错误日志上报给服务器
    logErrorToMyService(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      // 你可以自定义降级后的 UI 并渲染
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children; 
  }
}
```

##### 注意事项

&emsp; &emsp; 错误边界无法捕获以下场景中产生的错误：
&emsp; &emsp; 1. 事件处理
&emsp; &emsp; 2. 异步代码（例如 setTimeout 或 requestAnimationFrame 回调函数）
&emsp; &emsp; 3. 服务端渲染
&emsp; &emsp; 4. 它自身抛出来的错误（并非它的子组件）

##### 错误未被错误边界捕获时

&emsp; &emsp; 自 `React 16` 起，任何未被错误边界捕获的错误将会导致整个 `React` 组件树被卸载。

--------

* #### Fragments

&emsp; &emsp; `React` 中的一个常见模式是一个组件返回多个元素。 `Fragments` 允许你将子列表分组，而无需向 `DOM` 添加额外节点。用法如下：

``` jsx
class Component extends React.Component {
  render() {
    return (
      <React.Fragment>
        <span>Hello</span>
        <span>World</span>
      </React.Fragment>
    );
  }
}
```

##### 短语法

&emsp; &emsp; `Fragment` 更常用的方式如下:

``` jsx
class Component extends React.Component {
  render() {
    return (
      <>
        <span>Hello</span>
        <span>World</span>
      </>
    );
  }
}
```

&emsp; &emsp; 使用显式 `<React.Fragment>` 语法声明的 `Fragment` 与空标签 `<></>` 的区别在于， `<React.Fragment>` 可以具有属性 `key` (目前为止， `key` 是唯一可以传给 `<React.Fragment>` 的属性)，运用场景如下：

``` jsx
function Glossary(props) {
  return (
    <dl>
      {props.items.map(item => (
        // 没有`key`，React 会发出一个关键警告
        <React.Fragment key={item.id}>
          <dt>{item.term}</dt>
          <dd>{item.description}</dd>
        </React.Fragment>
      ))}
    </dl>
  );
}
```

--------

* #### 高阶组件

##### 不要在 `render` 方法中使用 `HOC`

&emsp; &emsp; `React` 的 `diff` 算法使用组件标识来确定它是应该更新现有子树还是将其丢弃并挂载新子树。 如果从 `render` 返回的组件与前一个渲染中的组件相同（===），则 `React` 通过将子树与新子树进行区分来递归更新子树。 如果它们不相等，则完全卸载前一个子树。
&emsp; &emsp; 通常，你不需要考虑这点。但对 `HOC` 来说这一点很重要，因为这代表着你不应在组件的 `render` 方法中对一个组件应用 `HOC` ，代码实例如下：

``` jsx
render() {
  // 每次调用 render 函数都会创建一个新的 EnhancedComponent
  // EnhancedComponent1 !== EnhancedComponent2
  const EnhancedComponent = enhance(MyComponent);
  // 这将导致子树每次渲染都会进行卸载，和重新挂载的操作！
  return <EnhancedComponent />;
}
```

&emsp; &emsp; 这不仅仅是性能问题 - 重新挂载组件会导致该组件及其所有子组件的状态丢失。
&emsp; &emsp; 如果在组件之外创建 `HOC` ，这样一来组件只会创建一次。因此，每次 `render` 时都会是同一个组件。一般来说，这跟你的预期表现是一致的。

--------

* #### 深入JSX

##### 在运行时选择类型

&emsp; &emsp; 你不能将通用表达式作为 `React` 元素类型。如果你想通过通用表达式来（动态）决定元素类型，你需要首先将它赋值给大写字母开头的变量。这通常用于根据 `prop` 来渲染不同组件的情况下:

``` jsx
import React from 'react'; 
import { PhotoStory, VideoStory } from './stories'; 

const components = {
  photo: PhotoStory, 
  video: VideoStory
}; 

function Story(props) {
  // 错误！JSX 类型不能是一个表达式。
  return <components[props.storyType] story={props.story} />; 
}
```

要解决这个问题, 需要首先将类型赋值给一个大写字母开头的变量：

``` jsx
import React from 'react'; 
import { PhotoStory, VideoStory } from './stories'; 

const components = {
  photo: PhotoStory, 
  video: VideoStory
}; 

function Story(props) {
  // 正确！JSX 类型可以是大写字母开头的变量。
  const SpecificStory = components[props.storyType]; 
  return <SpecificStory story={props.story} />; 
}
```

##### `Props` 默认值为 `“True”`

&emsp; &emsp; 如果你没给 `prop` 赋值，它的默认值是 `true` 。以下两个 `JSX` 表达式是等价的：

``` jsx
<MyTextBox autocomplete />

<MyTextBox autocomplete={true} />
```

&emsp; &emsp; 通常，不建议不传递 `value` 给 `prop` ，因为这可能与 `ES6` 对象简写混淆， `{foo}` 是 `{foo: foo}` 的简写，而不是 `{foo: true}` 。<span style="color:red">这样实现只是为了保持和 <strong>HTML</strong> 中标签属性的行为一致。</span>

##### JSX子元素 --- 字符串字面量

&emsp; &emsp; 包含在开始和结束标签之间的 `JSX` 表达式内容将作为特定属性 `props.children` 传递给外层组件（ `React` 不会对标签中间的内容进行转义）。例如：

``` jsx
function Component(props){
  console.log(props.children) 
  //log: {$$typeof: Symbol(react.element), type: "div", key: null, ref: null, props: {…}, …}
  return(
    <span>content</span>
  )
}
...
<Component><div style="color:red">dom of Component</div></Component>
...
```

##### JSX子元素 --- 空格以及空行的处理

&emsp; &emsp; `JSX` 会移除行首尾的空格以及空行。与标签相邻的空行均会被删除，文本字符串之间的新行会被压缩为一个空格。因此，以下的几种方式都是等价的：

``` html
<div>Hello World</div>

<div>
    Hello World
</div>

<div>
    Hello
    World
</div>

<div>

    Hello World
</div>
```

##### JSX子元素 --- 布尔类型、Null 以及 Undefined 将会忽略

&emsp; &emsp; `false` , `null` , `undefined` 以及 `true` 都是合法的子元素。但它们并不会被渲染。以下 `JSX` 表达式渲染结果相同：

``` html
<div />

<div></div>

<div>{false}</div>

<div>{null}</div>

<div>{undefined}</div>

<div>{true}</div>
```

这有助于依据特定条件来渲染其他的 `React` 元素。例如，在以下 `JSX` 中，仅当 `showHeader` 为 `true` 时，才会渲染 `<Header />` 组件：

``` jsx
<div>
  {showHeader && <Header />}
  <Content />
</div>
```

值得注意的是有一些 `“falsy”` 值，如数字 `0` ，仍然会被 `React` 渲染。例如，以下代码并不会像你预期那样工作，因为当 `props.messages` 是空数组时， `0` 仍然会被渲染：

``` jsx
<div>
  {props.messages.length &&
    <MessageList messages={props.messages} />
  }
</div>
```

要解决这个问题，确保 `&&` 之前的表达式总是布尔值：

``` jsx
<div>
  {props.messages.length > 0 &&
    <MessageList messages={props.messages} />
  }
</div>
```

反之，如果你想渲染 `false、true、null、undefined` 等值，你需要先将它们转换为字符串：

``` jsx
<div>
  My JavaScript variable is {String(myVariable)}.
</div>
```

--------

* #### Portals

&emsp; &emsp; `Portal` 提供了一种将子节点渲染到存在于父组件以外的 `DOM` 节点的优秀的方案。尽管 `portal` 可以被放置在 `DOM` 树中的任何地方，但在任何其他方面，其行为和普通的 `React` 子节点行为一致。由于 `portal` 仍存在于 `React` 树， 且与 `DOM` 树 中的位置无关，那么无论其子节点是否是 `portal` ，像 `context` 这样的功能特性都是不变的。代码实例如下:

``` jsx
//index.html
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Context and Portals</title>
</head>
<body>
  <div id="root"></div>
  <div id="another-root"></div>
</body>
</html>

//App.jsx
const anotherRoot = document.getElementById('another-root');
const ContextComponent = new React.createContext({});

class AnotherApp extends React.Component {
  render() {
    return (
      <ContextComponent.Consumer>
        {
          ({ str }) => (
            ReactDOM.createPortal(<span>{str}</span>, anotherRoot)
          )
        }
      </ContextComponent.Consumer>
    )
  }
}

export default class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <ContextComponent.Provider value={{ str: 'content from context' }}>
          <span>content in root</span>
          <AnotherApp />
        </ContextComponent.Provider>
      </React.Fragment>
    );
  }
}
```

运行结果截图如下:
{% asset_img react-recording-1.png %}&emsp; &emsp; 除此之外，事件冒泡也不是根据 `DOM` 树进行冒泡，而是根据 `React` 树进行冒泡。一个从 `portal` 内部触发的事件会一直冒泡至包含 `React` 树的祖先，即便这些元素并不是 `DOM` 树 中的祖先。<span style="color:red; ">这一特性可以很好的应用在弹窗组件的开发上。</span>

--------

* #### Profile

&emsp; &emsp; `Profiler` 测量渲染一个 `React` 应用多久渲染一次以及渲染一次的“代价”。 它的目的是识别出应用中渲染较慢的部分，并对其作出优化。 `Profiler` 能添加在 `React` 树中的任何地方来测量树中这部分渲染所带来的开销。 它需要两个 `prop` ：一个是 `id(string)` ，一个是当组件树中的组件“提交”更新的时候被 `React` 调用的回调函数 `onRender(function)` 。具体用法可参考[官方文档](https://zh-hans.reactjs.org/docs/profiler.html)

--------

* #### Refs & DOM

##### 访问Refs

&emsp; &emsp; `ref` 的值根据节点的类型而有所不同：

* 当 `ref` 属性用于 `HTML` 元素时，构造函数中使用 `React.createRef()` 创建的 `ref` 接收底层 `DOM` 元素作为其 `current` 属性。
* 当 `ref` 属性用于自定义 `class` 组件时，`ref` 对象接收组件的挂载实例作为其 `current` 属性。
* 你不能在函数组件上使用 `ref` 属性，因为他们没有实例。

&emsp; &emsp; `React` 会在组件挂载时给 `current` 属性传入 `DOM` 元素，并在组件卸载时传入 `null` 值。 `ref` 会在 `componentDidMount` 或 `componentDidUpdate` 生命周期钩子触发前更新。代码实例如下:

``` jsx
class ClassComp extends React.Component {
  divRef = React.createRef();
  pRef = React.createRef();
  render() {
    return (
      <>
        <div ref={this.divRef}>
          class component div
        </div>
        <p ref={this.pRef}>
          class component p
        </p>
      </>
    )
  }
}
export default class App extends React.Component {
  compRef = React.createRef();
  divRef = React.createRef();

  componentDidMount() {
    console.log(this.compRef.current);
    console.log(this.divRef.current)
  }

  render() {
    return (
      <>
        <ClassComp ref={this.compRef} />
        <div ref={this.divRef}>test content</div>
      </>
    );
  }
}
```

实现截图如下:{% asset_img refs&dom.png %}

##### 关于回调refs

&emsp; &emsp; 如果 `ref` 回调函数是以内联函数的方式定义的，在更新过程中它会被执行两次，第一次传入参数 `null` ，然后第二次会传入参数 `DOM` 元素。这是因为在每次渲染时会创建一个新的函数实例，所以 `React` 清空旧的 `ref` 并且设置新的。通过将 `ref` 的回调函数定义成 `class` 的绑定函数的方式可以避免上述问题.

--------

* #### 严格模式（StrictMode）

&emsp; &emsp; `StrictMode` 是一个用来突出显示应用程序中潜在问题的工具。与 `Fragment` 一样， `StrictMode` 不会渲染任何可见的 `UI` 。它为其后代元素触发额外的检查和警告。 `StrictMode` 目前有助于：

* 识别不安全的生命周期
* 关于使用过时字符串 `ref API` 的警告
* 关于使用废弃的 `findDOMNode` 方法的警告
* 检测意外的副作用
* 检测过时的 `context API`

未来的 `React` 版本将添加更多额外功能。

> 严格模式检查仅在开发模式下运行；它们不会影响生产构建。

--------

### API REFERENCE

* #### React

##### React. PureComponent

&emsp; &emsp; `React.PureComponent` 与 `React.Component` 很相似。两者的区别在于 `React.Component` 并未实现 `shouldComponentUpdate()` ，而 `React.PureComponent` 中以浅层对比 `prop` 和 `state` 的方式来实现了该函数。
&emsp; &emsp; 如果赋予 `React` 组件相同的 `props` 和 `state` ， `render()` 函数会渲染相同的内容，那么在某些情况下使用 `React.PureComponent` 可提高性能。

> `React.PureComponent` 中的 `shouldComponentUpdate()` 仅作对象的浅层比较。如果对象中包含复杂的数据结构，则有可能因为无法检查深层的差别，产生错误的比对结果。仅在你的 `props` 和 `state` 较为简单时，才使用 `React.PureComponent` ，或者在深层数据结构发生变化时调用 `forceUpdate()` 来确保组件被正确地更新。也可以考虑使用 `immutable` 对象加速嵌套数据的比较。

> 此外， `React.PureComponent` 中的 `shouldComponentUpdate()` 将跳过所有子组件树的 `prop` 更新。因此，请确保所有子组件也都是“纯”的组件。

##### React.memo

&emsp; &emsp; `React.memo` 为高阶组件。如果你的组件在相同 `props` 的情况下渲染相同的结果，那么你可以通过将其包装在 `React.memo` 中调用，以此通过记忆组件渲染结果的方式来提高组件的性能表现。这意味着在这种情况下， `React` 将跳过渲染组件的操作并直接复用最近一次渲染的结果。
&emsp; &emsp; `React.memo` 仅检查 `props` 变更。如果函数组件被 `React.memo` 包裹，且其实现中拥有 `useState` 或 `useContext` 的 `Hook` ，当 `context` 发生变化时，它仍会重新渲染。
&emsp; &emsp; 默认情况下其只会对复杂对象做浅层对比，如果你想要控制对比过程，那么请将自定义的比较函数通过第二个参数传入来实现。

``` js
function MyComponent(props) {
    /* 使用 props 渲染 */
}

function areEqual(prevProps, nextProps) {
    /*
    如果把 nextProps 传入 render 方法的返回结果与
    将 prevProps 传入 render 方法的返回结果一致则返回 true，
    否则返回 false
    */
}
export default React.memo(MyComponent, areEqual);
```

此方法仅作为性能优化的方式而存在。但请不要依赖它来“阻止”渲染，因为这会产生 bug。

> 与 `class` 组件中 `shouldComponentUpdate()` 方法不同的是，如果 `props` 相等， `areEqual` 会返回 `true` ；如果 `props` 不相等，则返回 `false` 。这与 `shouldComponentUpdate` 方法的返回值相反。

##### React.isValidElement

&emsp; &emsp; 验证对象是否为 `React` 元素，返回值为 `true` 或 `false` 。

``` js
React.isValidElement(object)
```

##### React.lazy & React. Suspense

&emsp; &emsp; `React.lazy()` 允许你定义一个动态加载的组件。这有助于缩减 `bundle` 的体积，并延迟加载在初次渲染时未用到的组件。 `React.Suspense` 可以指定 `加载指示器（loading indicator）` ，以防其组件树中的某些子组件尚未具备渲染条件。目前，懒加载组件是 `<React.Suspense>` 支持的唯一用例

> 渲染 `lazy` 组件依赖该组件渲染树上层的 `<React.Suspense>` 组件。这是指定 `加载指示器（loading indicator）` 的方式。

``` jsx
const OtherComponent = React.lazy(() => import('./OtherComponent'));

function MyComponent() {
  return (
    // 显示 <Spinner> 组件直至 OtherComponent 加载完成
    <React.Suspense fallback={<Spinner />}>
      <div>
        <OtherComponent />
      </div>
    </React.Suspense>
  );
}
```

* #### React. Component

&emsp; &emsp; 组件生命周期图谱：{% asset_img life-cycle.png %}

##### constructor()

&emsp; &emsp; 如果不初始化 `state` 或不进行方法绑定，则不需要为 `React` 组件实现构造函数。
&emsp; &emsp; 在 `React` 组件挂载之前，会调用它的构造函数。在为 `React.Component` 子类实现构造函数时，应在其他语句之前前调用 `super(props)` 。否则， `this.props` 在构造函数中可能会出现未定义的 `bug` 。
&emsp; &emsp; 在 `constructor()` 函数中不要调用 `setState()` 方法。如果你的组件需要使用内部 `state` ，请直接在构造函数中为 `this.state` 赋值初始 `state` 。
&emsp; &emsp; 要避免在构造函数中引入任何副作用或订阅。如遇到此场景，请将对应的操作放置在 `componentDidMount` 中。

> 关于为什么需要在 `constructor` 中使用 `super(props)` 可参考https://overreacted.io/zh-hans/why-do-we-write-super-props/

##### componentDidMount()

&emsp; &emsp; 你可以在 `componentDidMount()` 里直接调用 `setState()` 。它将触发额外渲染，但此渲染会发生在浏览器更新屏幕之前。如此保证了即使在 `render()` 两次调用的情况下，用户也不会看到中间状态。请谨慎使用该模式，因为它会导致性能问题。

##### forceUpdate()

&emsp; &emsp; 默认情况下，当组件的 `state` 或 `props` 发生变化时，组件将重新渲染。如果 `render()` 方法依赖于其他数据，则可以调用 `forceUpdate()` 强制让组件重新渲染。
&emsp; &emsp; 调用 `forceUpdate()` 将致使组件调用 `render()` 方法，此操作会跳过该组件的 `shouldComponentUpdate()` 。但其子组件会触发正常的生命周期方法，包括 `shouldComponentUpdate()`  `方法。如果标记发生变化，React` 仍将只更新 `DOM` 。
&emsp; &emsp; 通常应该避免使用 `forceUpdate()` ，尽量在 `render()` 中使用 `this.props` 和 `this.state` 。

##### Class属性

###### defaultProps

&emsp; &emsp; `defaultProps` 可以为 `Class` 组件添加默认 `props` 。这一般用于 `props` 未赋值，但又不能为 `null` 的情况。例如：

``` jsx
class CustomButton extends React.Component {
  // ...
}

CustomButton.defaultProps = {
  color: 'blue'
};
```

如果未提供 `props.color` ，则默认设置为 `'blue'`

``` jsx
  render() {
    return <CustomButton /> ; // props.color 将设置为 'blue'
  }
```

如果 `props.color` 被设置为 `null` ，则它将保持为 `null`

``` jsx
  render() {

    return <CustomButton color={null} /> ; // props.color 将保持是 null

  }
```

#### ReactDOM

##### render()

&emsp; &emsp; `ReactDOM.render(element, container[, callback])` 在提供的 `container` 里渲染一个 `React` 元素，并返回对该组件的引用（或者针对无状态组件返回 `null` ）。
&emsp; &emsp; 如果 `React` 元素之前已经在 `container` 里渲染过，这将会对其执行更新操作，并仅会在必要时改变 `DOM` 以映射最新的 `React` 元素。
&emsp; &emsp; 如果提供了可选的回调函数，该回调将在组件被渲染或更新之后被执行。

> `ReactDOM.render()` 会控制你传入容器节点里的内容。当首次调用时，容器节点里的所有 `DOM` 元素都会被替换，后续的调用则会使用 `React` 的 `DOM` 差分算法 `（DOM diffing algorithm）` 进行高效的更新。

> `ReactDOM.render()` 不会修改容器节点（只会修改容器的子节点）。可以在不覆盖现有子节点的情况下，将组件插入已有的 `DOM` 节点中。

> `ReactDOM.render()` 目前会返回对根组件 `ReactComponent` 实例的引用。 但是，目前应该避免使用返回的引用，因为它是历史遗留下来的内容，而且在未来版本的 `React` 中，组件渲染在某些情况下可能会是异步的。 如果你真的需要获得对根组件 `ReactComponent` 实例的引用，那么推荐为根元素添加 `callback ref` 。

##### unmountComponentAtNode()

&emsp; &emsp; `ReactDOM.unmountComponentAtNode(container)` 从 `DOM` 中卸载组件，会将其事件处理器（ `event handlers）` 和 `state` 一并清除。如果指定容器上没有对应已挂载的组件，这个函数什么也不会做。如果组件被移除将会返回 `true` ，如果没有组件可被移除将会返回 `false` 。

> `ReactDOM.unmountComponentAtNode()` 只能移除通过 `ReactDOM.render()` 挂载到 `DOM` 上的节点

--------

### HOOK

&emsp; &emsp; `Hook` 是一些可以让你在函数组件里“钩入” `React state` 及生命周期等特性的函数。

#### State Hook

&emsp; &emsp; `useState` 的使用形式如下：

``` js
const [fruit, setFruit] = useState('banana');
```

等式左边使用了数组解构。它意味着我们同时创建了 `fruit` 和 `setFruit` 两个变量， `fruit` 的值为 `useState` 返回的第一个值， `setFruit` 是返回的第二个值。它等价于下面的代码：

``` js
var fruitStateVariable = useState('banana'); // 返回一个有两个元素的数组
var fruit = fruitStateVariable[0]; // 数组里的第一个值
var setFruit = fruitStateVariable[1]; // 数组里的第二个值
```

当使用 `useState` 定义 `state` 变量时候，它返回一个有两个值的数组。第一个值是当前的 `state` ，第二个值是更新 `state` 的函数。使用 `[0]` 和 `[1]` 来访问有点令人困惑，因为它们有特定的含义。这就是使用数组解构的原因。

#### Effect Hook

&emsp; &emsp; `React` 保证了每次运行 `useEffect` 的同时， `DOM` 都已经更新完毕。

#### Hook规则

&emsp; &emsp; `只在最顶层使用 Hook` 。不要在循环，条件或嵌套函数中调用 `Hook` ， 确保总是在你的 `React` 函数的最顶层以及任何 `return` 之前调用他们。遵守这条规则，你就能确保 `Hook` 在每一次渲染中都按照同样的顺序被调用。这让 `React` 能够在多次的 `useState` 和 `useEffect` 调用之间保持 `hook` 状态的正确。

> 关于为什么需要确保 `Hook` 在每次渲染都保持一样的调用顺序，可参考：[无意识设计-复盘React Hook的创造过程](https:/github.com/shanggqm/blog/issues/4)、[深度理解Hook规则](https://www.codenong.com/s1190000022341755/)

#### Hook API

##### useCallback

&emsp; &emsp; `useCallback` 常见的使用场景：

* 当这个函数会被传递给子组件，为了避免子组件频繁渲染，使用 `useCallback` 包裹，保持引用不变；
* 需要保存一个函数闭包结果，如配合 `debounce`、`throttle` 使用；

##### useEffect 和 useLayoutEffect 的区别

&emsp; &emsp; 参考[深入理解 React useLayoutEffect 和 useEffect 的执行时机](https://blog.csdn.net/yunfeihe233/article/details/106616674)
