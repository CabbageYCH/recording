---
title: ts相关内容整理
date: 2021-05-10 23:36:31
tags: [前端, TypeScript, 官方文档整理]
categories: TypeScript
---
 &emsp; &emsp; 根据[官方文档](https://www.typescriptlang.org/docs/handbook/2/basic-types.html)，对TypeScript内容进行系统性梳理。

## The Basics

### Emitting with Errors

&emsp; &emsp; 运行 `tsc` 命令的时候，即使 `ts` 文件报错，还是会被编译成对应的 `js` 文件，想要在 `ts` 文件报错的情况下不编译成 `js` 文件，需要带上参数 `--noEmitOnError` 。

```ts
tsc --noEmitOnError hello.ts
```

### Downleveling

&emsp; &emsp; 进行编译时，带上参数 `--target es2015(或是其它js版本)` ，可以将 `ts` 编译成指定版本的 `js` 代码。

```ts
tsc --target es2015 hello.ts
```

### Strictness

&emsp; &emsp; `CLI` 中的 `--strict` 标志或 `tsconfig.json` 中的 `"strict"：true` 会开启严格模式。但我们可以单独选择检查内容中的某一项不进行严格检查。其中应该知道的两个最大的对象是 `noImplicitAny` 和 `strictNullChecks` 。

#### noImplicitAny

&emsp; &emsp; 开启 `noImplicitAny` ，将对类型隐式推断为 `any` 的任何变量发出错误。

#### strictNullChecks

&emsp; &emsp; `strictNullChecks` 标志使对 `null` 和 `undefined` 的处理更加明确，并使我们不必担心我们是否忘记了对 `null` 和 `undefined` 的处理。在该模式下， `null` 和 `undefined` 不属于任何一个类型，它们只能赋值给自己这种类型或者 `any` (有一个例外， `undefined` 也可以赋值给 `void` )。因此，在常规类型检查模式下 `T` 和 `T | ndefined` 被认为是等同的（因为 `undefined` 被看作 `T` 的子类型），但它们在严格类型检查模式下是不同的类型，只有 `T | undefined` 类型允许出现 `undefined` 值。 `T` 和 `T | null` 也是这种情况。

--------

## Everyday Types

### The primitives: string, number, and boolean

&emsp; &emsp; `string` , `number` , `boolean` 的首字母均为小写，不要误写成大写（JS内置类型）。

### Type Aliases

&emsp; &emsp; 当某个自定义类型需要在代码中多处用到时，可以使用类型别名，即为自定义类型命名。代码示例如下：

```ts
type Point = {
    x: number;
    y: number;
};
const a: Point = {
    x: 1,
    y: 2,
}
const fn = (arg: Point) => '';

// 联合类型
type ID = number | string;
const p1: ID = 'string';
const p2: ID = 0x123;
```

### Interfaces

&emsp; &emsp; 接口是用来给对象定义类型的另一种方式，代码示例如下：

```ts
// 类型别名
type Point1 = {
    x: number;
    y: number;
};

// 接口
interface Point2 {
    x: number;
    y: number;
}
```

&emsp; &emsp; `Interface` 和 `type` 非常相似，主要区别有以下两点：

1. 继承的方式不同， `interface` 通过 `extend` 进行继承， `type` 通过 `&` 进行继承，代码如下：

```ts
const getBear = () => ({ name: 'bear', honey: false });

// interface
interface Animal {
    name: string
}
interface Bear extends Animal {
    honey: boolean
}
const bear = getBear()
bear.name
bear.honey

// type
type AnimalType = {
    name: string
}
type BearType = AnimalType & {
    honey: boolean
}
const bearType = getBear()
bearType.name
bearType.honey
```

2. `type`在被定义后不可扩展，代码如下：

```ts
// interface
interface Window {
    title: string;
}
interface Window {
    add: number;
}

const win: Window = {
    title: 'title',
    add: 1,
}

// type
type WindowType {
    title: string;
}
type WindowType {  // err: Duplicate identifier 'WindowType'.ts(2300)
    add: number;
}
```

### Type Assertions

&emsp; &emsp; `TypeScript` 只允许类型断言将某个类型转换为更（不）具体的类型，此规则可防止一些错误的类型断言，例如：

```ts
const b = 'string' as any;

type objType = {
    name: string;
    age: number;
}
const obj = { name: 'obj', age: 123 } as objType;

const a = 'string' as number;
/**
 * err: Conversion of type 'string' to type 'number' may be a mistake because neither type sufficiently
 * overlaps with the other. If this was intentional, convert the expression to 'unknown' first.ts(2352)
 */
```

&emsp; &emsp; 有时，此规则可能过于保守，导致一些可能有效但更复杂的强制转换不被允许。如果发生这种情况，可以使用两个断言，首先是 any（或unknown），然后是所需的类型：

```ts
const a = (expr as any) as T;
```

### Literal Types

&emsp; &emsp; 文字类型一般很少单独使用，大多是和联合类型结合起来：

```ts
type literalType = 'string-1' | 'string-2' | 'string-3';

const a: literalType = 'string-1';

const b: literalType = 'stirng2';
// err: Type '"stirng2"' is not assignable to type 'literalType'.ts(2322)
```

> `boolean` 可以看作是一种特殊的文字类型，即： `type boolean = false | true`

### Literal Inference

&emsp; &emsp; 初始化一个对象变量的时候， `TS` 会认为对象的属性值会被改变，当发生以下操作时：

```ts
const obj = { counter: 0 };
if (someCondition) {
    obj.counter = 1;
}
```

`counter` 属性的值从0变成1，由于两者都是 `number` 类型， `TS` 不会报错。但是，当碰上如下这种文字类型的情况时：

```ts
const handleRequest = (url: string, method: 'POST' | 'GET') => '';

const req = { url: "https://example.com", method: "GET" };
handleRequest(req.url, req.method);
// err: Argument of type 'string' is not assignable to parameter of type '"POST" | "GET"'.ts(2345)
```

因为 `TS` 认为 `req.method` 的值有可能会被赋值成除"GET"和"POST"以外的值，所以给出错误提示。对于这种情况，有以下两种对应方法，方法一：

```ts
// 使用类型断言
const req3 = { url: "https://example.com", method: "GET" as 'POST' | 'GET' };
handleRequest(req3.url, req3.method);

// 或是
const req4 = { url: "https://example.com", method: "GET" };
handleRequest(req4.url, req4.method as 'POST' | 'GET');
```

方法二：

```ts
// 使用 as const后缀
const req2 = { url: "https://example.com", method: "GET" } as const;
handleRequest(req2.url, req2.method);
```

`as const` 后缀将 `req2` 的属性状态都变成 `readonly` ，确保不会被改变。

### null and undefined

&emsp; &emsp; 非空断言（后缀 `!` ）：

```ts
const fn = (arg: null | string) => {
    const a: string = arg;
    // err: Type 'string | null' is not assignable to type 'string'.

    const b: string = arg!;
}
```

### Enums

&emsp; &emsp; 枚举是 `TypeScript` 添加到 `JavaScript` 的一项特性。

#### Numeric enums

&emsp; &emsp; 数字型枚举特性如下：

```ts
enum Direction {
    Up,
    Down,
    Left,
    Right,
}
console.log(Direction);
// log: { '0': 'Up', '1': 'Down', '2': 'Left', '3': 'Right', Up: 0, Down: 1, Left: 2, Right: 3 }

enum Direction2 {
    Up = 3,
    Down,
    Left,
    Right,
}
console.log(Direction2)
// log: { '3': 'Up', '4': 'Down', '5': 'Left', '6': 'Right', Up: 3, Down: 4, Left: 5, Right: 6 }

enum Direction3 {
    Up,
    Down,
    Left = 5,
    Right,
}
console.log(Direction3)
// log: { '0': 'Up', '1': 'Down', '5': 'Left', '6': 'Right', Up: 0, Down: 1, Left: 5, Right: 6 }
```

数字型枚举默认对枚举值从0开始递增赋值，对于赋初始值的枚举值，后续的枚举值依次递增。对于没有初始化的枚举值，要么处于首位（默认为0），要么必须跟在数字常量初始化的数字枚举之后，如下这种情况不允许出现：

```ts
const fn = () => 1;

enum NumberEnum {
    first = fn(),
    second,
    // err: Enum member must have initializer.ts(1061)
}
```

#### String enums

&emsp; &emsp; 字符串枚举类型，每个枚举值都必须赋初始值：

```ts
enum Direction {
    Up = "UP",
    Down = "DOWN",
    Left, // err: Enum member must have initializer.ts(1061)
    Right = "RIGHT",
}
```

#### Heterogeneous enums

&emsp; &emsp; 一个枚举类型中，枚举值可以是数字和字符串同时存在，但是这么做并不推荐：

```ts
enum BooleanLikeHeterogeneousEnum {
    No = 0,
    Yes = "YES",
}
```

--------

### Less Common Primitives

#### bigint(ES2020)

&emsp; &emsp; `TS` 使用小写 `bigint` 表示Bigint类型：

```ts
// Creating a bigint via the BigInt function
const oneHundred: bigint = BigInt(100);

// Creating a BigInt via the literal syntax
const anotherHundred: bigint = 100n;
```

#### symbol

代码示例如下：

```ts
const a: symbol = Symbol('symbol')
const b: symbol = Symbol('symbol')
console.log(a === b) // false
```

### unknown

&emsp; &emsp; 对于需要运行时才能确定的类型，可以用 `unknown` 表示：

```ts
let notSure: unknown = 4;
notSure = "maybe a string instead"; // works
notSure = false; // works
const num: number = notSure; // err: Type 'unknown' is not assignable to type 'number'.ts(2322)
if (typeof notSure === 'number') {
    const num2: number = notSure; // works
}
```

与 `any` 不同，在不能明确 `unknown` 类型所对应值的具体类型时，其它类型的值不可以被赋值给 `unknown` 。

--------

## Narrowing

### Using type predicates

&emsp; &emsp; 类型谓词表现如下：

```ts
type Fish = {
    swim: () => string;
}

type Bird = {
    fly: () => string;
}

function isFish(pet: Fish | Bird): pet is Fish {
    return (pet as Fish).swim !== undefined;
}
```

`isFish` 函数的返回值（ `parameterName is Type` ）含义：当函数的 `return语句` 返回 `true` 时，传入的参数 `pet` 是 `Fish` 类型。应用如下：

```ts
function isFish(pet: Fish | Bird): pet is Fish {
    return (pet as Fish).swim !== undefined;
}

function getSmallPet() {
    return { fly: () => 'bird fly' }
}

let pet = getSmallPet();

if (isFish(pet)) {
    pet.swim();
} else {
    pet.fly();
}
```

对比不用类型谓词：

```ts
function isFish(pet: Fish | Bird) {
    return (pet as Fish).swim !== undefined;
}

let pet = getSmallPet();

if (isFish(pet)) {
    pet.swim(); // err: Property 'swim' does not exist on type '{ fly: () => string; }'.ts(2339)
} else {
    pet.fly();
}
```

### The never type

&emsp; &emsp; `never` 类型可以赋值给任意类型，但是反过来不可行，只有 `never` 类型可以赋值给 `never` 类型。

```ts
const fn = (arg: string | number) => {
    switch (typeof arg) {
        case 'string': return 'string';
        case 'number': return 'number';
        default:
            let c: never = arg;
            let b: number = c;
            c = b; // err: Type 'number' is not assignable to type 'never'.ts(2322)
            return c;
    }
}
```

--------

## More on Functions

### Call Signatures

&emsp; &emsp; 调用签名用于声明带属性的函数类型：

```ts
type DescribableFunction = {
    description: string;
    (someArg: number): boolean;
};

const fn = (arg: number) => arg > 2;
fn.description = 'fn.description';

function doSomething(fn: DescribableFunction) {
    console.log(fn.description + " returned " + fn(6));
}

doSomething(fn); // log: fn.description returned true
```

### Construct Signatures

&emsp; &emsp; 声明需要使用 `new` 运算符调用的函数时，可以使用构造签名：

```ts
class SomeObject {
    private str: string;
    constructor(str: string) {
        this.str = str;
        console.log(`str: ${str}`);
    }
}

type SomeConstructor = {
    new(s: string): SomeObject;
};

function fn(ctor: SomeConstructor) {
    return new ctor("hello");
}
fn(SomeObject); // log: str: hello
```

### Generic Functions

&emsp; &emsp; 通用函数使用如下：

```ts
function firstElement<Type>(arr: Type[]): Type {
    return arr[0];
}
// s is of type 'string'
const s = firstElement(["a", "b", "c"]);
console.log(s); // log: a

// n is of type 'number'
const n = firstElement([1, 2, 3]);
console.log(n); // log: 1
```

### Constraints

&emsp; &emsp; 想要对通用函数的参数类型加以限制，可以使用 `extends` 添加约束：

```ts
function longest<Type extends { length: number }>(a: Type, b: Type) {
    if (a.length >= b.length) {
        return a;
    } else {
        return b;
    }
}

const longerArray = longest([1, 2], [1, 2, 3]);
console.log(longerArray);
// log: [ 1, 2, 3 ]

const longerString = longest("alice", "bob");
console.log(longerString);
// log: article

//const notOK = longest(10, 100);
// err: Argument of type 'number' is not assignable to parameter of type '{ length: number; }'.ts(2345)
```

### Function Overloads

&emsp; &emsp; 在 `TypeScript` 中，我们可以通过编写重载签名来指定一个可以以不同方式调用的函数。函数重载有以下规则：1、重载签名后紧跟实现签名。2、实现签名必须兼容重载签名。如下：

```ts
// 违反规则1
function makeDate(timestamp: number): Date;
function makeDate(m: number, d: number, y: number): Date;
// err: Function implementation is missing or not immediately following the declaration.ts(2391)
const str = 'test';
function makeDate(mOrTimestamp: number, d?: number, y?: number): Date {
    if (d !== undefined && y !== undefined) {
        return new Date(y, mOrTimestamp, d);
    } else {
        return new Date(mOrTimestamp);
    }
}

// 违反规则2
function makeDate(timestamp: number): Date;
function makeDate(m: number, d: number, y: number): Date;
function makeDate(mOrTimestamp: number, d?: number, y?: number): Date {
    if (d !== undefined && y !== undefined) {
        return new Date(y, mOrTimestamp, d);
    } else {
        return new Date(mOrTimestamp);
    }
}
const d1 = makeDate(12345678);
const d2 = makeDate(5, 5, 5);

const d3 = makeDate(1, 3);
// err: No overload expects 2 arguments, but overloads do exist that expect either 1 or 3 arguments.ts(2575)
```

### Writing Good Overloads

&emsp; &emsp; 好的函数重载实现例子：

```ts
function len(s: string): number;
function len(arr: any[]): number;
function len(x: any) {
  return x.length;
}
```

但是，我们不能使用可能是字符串或数组的值来调用它，因为 `TypeScript` 只能将函数调用解析为单个重载。继续以上的例子：

```ts
len(""); // OK
len([0]); // OK
len(Math.random() > 0.5 ? "hello" : [0]);
/**
err:
  No overload matches this call.
  Overload 1 of 2, '(s: string): number', gave the following error.
    Argument of type 'number[] | "hello"' is not assignable to parameter of type 'string'.
      Type 'number[]' is not assignable to type 'string'.
  Overload 2 of 2, '(arr: any[]): number', gave the following error.
    Argument of type 'number[] | "hello"' is not assignable to parameter of type 'any[]'.
      Type 'string' is not assignable to type 'any[]'.ts(2769)
 */
```

### Declaring `this` in a Function

&emsp; &emsp; 如下一段代码：

```ts
var name = 'global';
const obj = {
    name: "obj",
    getName() {
        return this.name
    },
}
const fn1 = obj.getName;
fn1(); // 在js中输出：global
```

在 `javascript` 中， `fn1()` 输出全局变量 `name` ，而不是输出预期的 `obj.name` ，而在ts中，可以在函数中声明 `this` 来解决这一问题：

```ts
var name = 'global';
type Obj = {
    name: string
    // 限定getName调用时的this类型
    getName(this: Obj): string
}
const obj: Obj = {
    name: "obj",
    getName() {
        return this.name
    },
}
const fn1 = obj.getName;
fn1();
// err: The 'this' context of type 'void' is not assignable to method's 'this' of type 'Obj'.ts(2684)
```

如果在 `Obj` 中使用了箭头函数导致 `getName` 绑定外层作用域的 `this` ， `ts` 会作如下提示：

```ts
type Obj = {
    name: string;
    getName(this: Obj): string
}
const obj: Obj = {
    name: "obj",
    getName: () => {
        return this.name;
        // err: The containing arrow function captures the global value of 'this'.ts(7041)
    },
}
```

### Other Types to Know About

#### void

&emsp; &emsp; `void` 表示函数没有返回值（与undefined不同）。

#### object

&emsp; &emsp; `object` 表示除了原始值（ `string, number, boolean, symbol, null, or undefined` ）以外的所有值。

> `object` 不是 `js` 内置类型 `Object` 。始终用 `object` 表示类型！

#### never

&emsp; &emsp; `never` 在函数中的两种使用情况：

```ts
// 情形一：
function fail(msg: string): never {
    throw new Error(msg);
}
```

`never` 类型表示从未观察到的值。在返回类型中，这意味着函数抛出异常或终止程序的执行。

```ts
// 情形二
function fn(x: string | number) {
    if (typeof x === "string") {
        // do something
    } else if (typeof x === "number") {
        // do something else
    } else {
        x; // has type 'never'!
    }
}
```

用来表示函数的参数值类型在联合类型之外。

#### Function

&emsp; &emsp; 用来表示传入的参数为函数：

```ts
function doSomething(f: Function) {
    f(1, 2, 3);
}
```

但这是一个无类型的函数调用，最好避免。

### Rest Parameters and Arguments

#### Rest Parameters（函数定义时的参数）

&emsp; &emsp; 在 `typescript` 中，使用 `...` 来表示参数个数不定：

```ts
function multiply(n: number, ...m: number[]) {
    return m.map((x) => n * x);
}
const a = multiply(10, 1, 2, 3, 4);
console.log(a) // log: [ 10, 20, 30, 40 ]
```

#### Rest Arguments（函数调用时的参数）

&emsp; &emsp; 函数调用时， `...` 的使用如下：

```ts
const arr1 = [1, 2, 3];
const arr2 = [4, 5, 6];
arr1.push(...arr2);
```

但是，ts不会假设数组是不可变的，因此，如下调用方式会报错：

```ts
const args = [8, 5];
const angle = Math.atan2(...args);
// err: Expected 2 arguments, but got 0 or more.ts(2556)
```

想要避免如上错误，需要使用 `const` 后缀对数组进行声明：

```ts
const args = [8, 5] as const;
const angle = Math.atan2(...args);
```

### Parameter Destructuring

&emsp; &emsp; `Typescript` 中的参数解构如下：

```ts
type ABC = { a: number; b: number; c: number };
function sum({ a, b, c }: ABC) {
    console.log(a + b + c);
}
sum({ a: 1, b: 2, c: 3 }) // log: 6
```

### Assignability of Functions

#### Return type void

&emsp; &emsp; 具有 `void` 返回类型（ `type vf = () => void` ）的上下文函数类型，在实现时，可以返回任何其他值，但会被忽略。因此，如下代码不会报错：

```ts
type voidFunc = () => void;
const f1: voidFunc = () => {
    return true;
};
const f2: voidFunc = () => true;
const f3: voidFunc = function () {
    return true;
};
```

而且，当返回值被赋给其它变量时， `ts` 会将被赋值的变量类型识别为 `void` ：

```ts
const a1 = f1(); // const a1: void
const a2 = f2(); // const a2: void
const a3 = f3(); // const a3: void
console.log(a1, a2, a3); // log: true true true
```

这一特性也是如下常用方法没有报错的原因：

```ts
const src = [1, 2, 3];
const dst = [0];
src.forEach((el) => dst.push(el));
```

`Array.forEach` 接受一个返回值为 `void` 的函数，而 `Array.push` 的返回值为 `number` 类型。没有报错是因为返回的 `number` 被忽略了。

#### special case

&emsp; &emsp; 当使用函数字面量或者是函数声明定义时，如果把函数的返回类型定义为 `void` ，则该函数不能有任何返回内容：

```ts
function f1(): void {
    return;
}
function f2(): void {
    return true; // err: Type 'boolean' is not assignable to type 'void'.ts(2322)
}
const f3 = function (): void {
    return 3; // err: Type 'number' is not assignable to type 'void'.ts(2322)
};
```

---------

## Object Types

### Optional Properties

&emsp; &emsp; 需要注意，在对象解构中，容易将类型定义与重定义混淆：

```ts
function draw({ shape: Shape, xPos: number = 100 }) {
    console.log(number) // log: 100
    console.log(shape);
    // err: Cannot find name 'shape'. Did you mean 'Shape'?ts(2552)
}

draw({ shape: 'circle' });
```

### readonly Properties

&emsp; &emsp; `ts` 中使用 `readonly` 声明只读属性：

```ts
interface Home {
    readonly resident: { name: string; age: number };
}
function visitForBirthday(home: Home) {
    // We can read and update properties from 'home.resident'.
    console.log(`Happy birthday ${home.resident.name}!`);
    home.resident.age++;
}
function evict(home: Home) {
    // But we can't write to the 'resident' property itself on a 'Home'.
    // err: Cannot assign to 'resident' because it is a read-only property.ts(2540)
    home.resident = {
        name: "Victor the Evictor",
        age: 42,
    };
}
```

> `ts` 在检查两个属性是否匹配时，不会把 `readonly` 考虑在内，因此会出现如下状况：

```ts
interface Person {
    name: string;
    age: number;
}

interface ReadonlyPerson {
    readonly name: string;
    readonly age: number;
}

let writablePerson: Person = {
    name: "Person McPersonface",
    age: 42,
};

// works
let readonlyPerson: ReadonlyPerson = writablePerson;

console.log(readonlyPerson.age); // prints '42'
writablePerson.age++;
console.log(readonlyPerson.age); // prints '43'
```

### Index Signature

&emsp; &emsp; 对于不知道属性名称，但确切知道类型的时候，可以使用类型签名：

```ts
interface objType {
    [index: string]: string;
}
const str: objType = {
    name: 'index signature',
    age: '18'
}
console.log(str); // log: { name: 'index signature', age: '18' }
```

其中， `index` 支持 `string` 与 `number` 两种类型。当索引为 `string` 类型时，该类型声明下的所有其它类型都必须与索引类型的返回值相同：

```ts
interface objType {
    [index: string]: string;
    other: number; // err: Property 'other' of type 'number' is not assignable to string index type 'string'.ts(2411)
}
```

> 由于 `js` 中的对象属性可以通过 `obj.property` 访问，也可以通过 `obj['property']` 进行访问。因此，当索引类型声明为 `[index: string]: string; ` 时，其余属性的类型如果与索引类型不同，会影响 `ts` 的判断。

### Extending Types

&emsp; &emsp; `ts` 中的类型继承可以同时继承多个：

```ts
interface Colorful {
    color: string;
}
interface Circle {
    radius: number;
}
interface ColorfulCircle extends Colorful, Circle { }
const cc: ColorfulCircle = {
    color: "red",
    radius: 42,
};
```

### Intersection Types

&emsp; &emsp; 交叉类型，取多个类型的并集，构造新的类型：

```ts
interface Colorful {
    color: string;
}
interface Circle {
    radius: number;
}
type test = {
    test: string;
}
type ColorfulCircle = Colorful & Circle & test;
const obj: ColorfulCircle = {
    color: 'red',
    radius: 666,
    test: 'test',
}
console.log(obj); // log: { color: 'red', radius: 666, test: 'test' }
```

### Generic Object Types

&emsp; &emsp; 使用 `Type` 参数创建范型类型如下：

```ts
interface Box<Type> {
    contents: Type;
}
interface StringBox {
    contents: string;
}
let boxA: Box<string> = { contents: "hello" }; // works
let boxB: StringBox = { contents: "world" }; // works

function setContents<Type>(box: Box<Type>, newContents: Type) {
    box.contents = newContents;
}
const Box: Box<number> = {
    contents: 666
}
const BoxString: Box<string> = {
    contents: '',
}
setContents(Box, 777); // works
setContents(BoxString, 'BoxString'); // works
```

改用类型别名实现如下：

```ts
type OrNull<Type> = Type | null;

type OneOrMany<Type> = Type | Type[];

type OneOrManyOrNull<Type> = OrNull<OneOrMany<Type>>;

type OneOrManyOrNull02<Type> = OneOrMany<Type> | null;

type OneOrManyOrNullStrings = OneOrManyOrNull<string>;
```

### The ReadonlyArray Type

&emsp; &emsp; `ReadonlyArray` 用于描述 `Readonly` 类型的数组：

```ts
function doStuff(values: ReadonlyArray<number>) {
    const copy = values.slice();
    console.log(`The first value is ${values[0]}`); // log: The first value is 1
    values[0] = 666; // err: Index signature in type 'readonly number[]' only permits reading.ts(2542)
    values.unshift(); // err: Property 'unshift' does not exist on type 'readonly number[]'.ts(2339)
    values.push("hello!"); // err: Property 'push' does not exist on type 'readonly number[]'.ts(2339)
}
doStuff([1, 2, 3, 4, 5, 6,])
```

`ReadonlyArray` 类型的数组，不可以改变数组个数，也不可以对数组元素的值进行改变。

### Tuple Types

&emsp; &emsp; 元组类型是一种特殊的数组类型，数组元素个数确定，以及每个位置的元素类型确定：

```ts
type StringNumberPair = [string, number];
const tupleType: StringNumberPair = ['type', 123];
function doSomething(pair: [string, number], pair2: StringNumberPair) {
    console.log(pair.length, pair[0]);
    console.log(pair[2]); // err: Tuple type '[string, number]' of length '2' has no element at index '2'.ts(2493)
}

doSomething(["hello", 42], ['hello', 666]);
```

> 元组类型可以通过数组方法对数组中的内容进行更改：

```ts
type StringNumberPair = [string, number];
function doSomething(pair: StringNumberPair) {
    pair.unshift(666);
    console.log(pair); // log: [ 666, 'hello', 42 ]
    console.log(pair[2]); // err: Tuple type 'StringNumberPair' of length '2' has no element at index '2'.ts(2493)
}

doSomething(["hello", 42]);
```

#### Optional tuple elements

```ts
type Either2dOr3d = [number, number, number?];
function setCoordinate(coord: Either2dOr3d) {
    const [x, y, z] = coord;
    console.log(z);
}
setCoordinate([1, 2]) // log: undefined
setCoordinate([1, 2, 3]) // log: 3
```

#### rest elements

```ts
type StringNumberBooleans = [string, number, ...boolean[]];
type StringBooleansNumber = [string, ...boolean[], number];
type BooleansStringNumber = [...boolean[], string, number];

const a: StringNumberBooleans = ["hello", 1];
const b: StringNumberBooleans = ["beautiful", 2, true];
const c: StringNumberBooleans = ["world", 3, true, false, true, false, true];

const d: StringBooleansNumber = ['hello', 3];
const e: StringBooleansNumber = ['hello', false, true, true, 3];
```

--------

## Type Manipulation

### Keyof Type Operator

&emsp; &emsp; `keyof` 返回对象属性 `（number or string）` 的联合类型：

```ts
type Point = { x: number; y: number };
type P = keyof Point;
function keyOfFn<T, K extends keyof T>(obj: T, key: K): T[K] {
    return obj[key]
}

console.log(keyOfFn({ name: 'cabbage' }, 'name')); // log: cabbage
```

对于有索引签名的对象， `keyof` 直接返回对应类型：

```ts
type Arrayish = { [n: number]: unknown };
type A = keyof Arrayish; 
// type A = number
type Mapish = { [k: string]: boolean };
type M = keyof Mapish;
// type M = string | number
```

上述例子中的 `M` ， 类型是 `string` 和 `number` 的联合的原因是：在 `js` 中，对象的 `key` 总是会被强制转换成 `string` 类型，即 `obj[0]` 等同于 `obj["0"]` 。

### Typeof Type Operator

> `js` 中已有 `typeof` 关键字， `ts` 中的 `typeof` 用于在类型定义时使用。

```ts
type StringOrNumber = string | number;
const str: StringOrNumber = 666;
type TypeOfType = typeof str;
// type TypeOfType = number
```

### Indexed Access Types

&emsp; &emsp; 索引访问类型的使用如下：

```ts
type Person = { age: number; name: string; alive: boolean };
type Age = Person["age"];
// type Age = number

type I1 = Person["age" | "name"];
// type I1 = string | number

type I2 = Person[keyof Person];
// type I2 = string | number | boolean

type AliveOrName = "alive" | "name";
type I3 = Person[AliveOrName];
// type I3 = string | boolean
```

使用 `number` 可以获取数组中元素的具体类型：

```ts
type Person = { age: number; name: string; alive: boolean }[];

type I1 = Person[number];
// type I1 = { age: number; name: string; alive: boolean; }
```

索引值不可以是具体变量，但可以是类型别名：

```ts
type Person = { age: number; name: string; alive: boolean };
const key = "age";
type Age = Person[key];
// err: Type 'any' cannot be used as an index type.ts(2538)
// err: 'key' refers to a value, but is being used as a type here. Did you mean 'typeof key'?ts(2749)

// works
type key2 = "age";
type Age2 = Person[key2];
```

### Conditional Types

&emsp; &emsp; `ts` 还可以使用条件语句进行类型定义，具体参考[Conditional Types](https://www.typescriptlang.org/docs/handbook/2/conditional-types.html)

### Template Literal Types

&emsp; &emsp; 在类型定义的时候，也可以使用模版字符串定义文字类型：

```ts
type World = "world";
type Greeting = `hello ${World}`;
// type Greeting = "hello world"

type EmailLocaleIDs = "welcome_email" | "email_heading";
type FooterLocaleIDs = "footer_title" | "footer_sendoff";
type AllLocaleIDs = `${EmailLocaleIDs | FooterLocaleIDs}_id`;
// type AllLocaleIDs = "welcome_email_id" | "email_heading_id" | "footer_title_id" | "footer_sendoff_id"
```

具体参考[Template Literal Types](https://www.typescriptlang.org/docs/handbook/2/template-literal-types.html)

### Mapped Types

&emsp; &emsp; `ts` 可以通过对已存在的类型属性进行遍历得到新的类型：

```ts
type OptionsFlags<Type> = {
    [Property in keyof Type]: boolean;
};
type FeatureFlags = {
    darkMode: () => void;
    newUserProfile: () => void;
};

type FeatureOptions = OptionsFlags<FeatureFlags>;
/*
type FeatureOptions = {
    darkMode: boolean;
    newUserProfile: boolean;
}
*/
```

在 `[Property in keyof Type]` 前加上前缀 `+` 或 `-` （默认是 `+` ），还可对类型进行修改：

```ts
type CreateMutable<Type> = {
    readonly [Property in keyof Type]: Type[Property];
};
type LockedAccount = {
    id: string;
    name: string;
};

type UnlockedAccount = CreateMutable<LockedAccount>;
/*
type UnlockedAccount = {
    readonly id: string;
    readonly name: string;
}
*/

type Concrete<Type> = {
    [Property in keyof Type]-?: Type[Property];
};
type MaybeUser = {
    id: string;
    name?: string;
    age?: number;
};

type User = Concrete<MaybeUser>;
/*
type User = {
    id: string;
    name: string;
    age: number;
}
*/
```

除此之外， `[Property in keyof Type]` 还可以搭配 `as` 使用，具体参考[Mapped Types](https://www.typescriptlang.org/docs/handbook/2/mapped-types.html)。

--------

## Classes

&emsp; &emsp; 不同于 `js` ， `ts` 不仅可以在构造函数之外对属性进行类型定义，还可以进行赋值：

```ts
// works
class Point {
    x: number = 1;
    y: number = 2;
}

const pt = new Point();
pt.x = 0;
pt.y = 0;
```

`x` , `y` 为公共可写属性。

### --strictPropertyInitialization

&emsp; &emsp; `strictPropertyInitialization` 设置是否需要在构造函数中初始化类字段（默认为true）：

```ts
class test {
    name: string;
}
// err: Property 'name' has no initializer and is not definitely assigned in the constructor.ts(2564)
```

也可以使用非空断言（后缀 `!` ）避免这一报错：

```ts
// works
class test {
    name!: string;
}
```

### Super Calls

&emsp; &emsp; 与 `js` 一样，在子类中使用 `this` 前需要调用 `super()` ：

```ts
class Base {
    k = 4;
}
class Derived extends Base {
    constructor() {
        console.log(this.k);
        // err: 'super' must be called before accessing 'this' in the constructor of a derived class.
        super();
    }
}
```

### Methods

&emsp; &emsp; 类中的方法需要通过 `this` 访问类中的变量：

```ts
let x: number = 0;

class C {
    x: string = "hello";
    m() {
        this.x = 'world';
        x = "world"; // err: Type 'string' is not assignable to type 'number'.ts(2322)
    }
}
```

### Index Signatures

&emsp; &emsp; 类中也可以使用索引签名进行类型声明：

```ts
class MyClass {
    [s: string]: boolean | ((s: string) => boolean);

    check(s: string) {
        return this[s] as boolean;
    }

    testFn(num: number) { // err: Property 'testFn' of type '(num: number) => string' is not assignable to string index type 'boolean | ((s: string) => boolean)'.ts(2411)
        return 'test';
    }
}
```

### Class Heritage

#### implements Clauses

&emsp; &emsp; `ts` 可以通过 `implements` 检查类是否实现某个（或多个）接口：

```ts
interface Pingable {
    ping(): void;
}
class Sonar implements Pingable {
    ping() {
        console.log("ping!");
    }
}
// works
class PingPong extends Sonar implements Pingable {
    pong() {
        console.log('pong!');
    }
}
class Ball implements Pingable {
    // err: Class 'Ball' incorrectly implements interface 'Pingable'.
    //      Property 'ping' is missing in type 'Ball' but required in type 'Pingable'.ts(2420)
    pong() {
        console.log("pong!");
    }
}
```

> `implements interface` 只会对类的实现进行检查，并不会对类本身定义的内容作任何改变。

#### Overriding Methods

&emsp; &emsp; `ts` 中的类可以通过 `super` 访问父类中的方法：

```ts
class Base {
    name: string = 'cabbage';
    greet() {
        console.log("Hello, world!");
    }
}

class Derived extends Base {
    greet(name?: string) {
        if (name === undefined) {
            super.greet();
        } else {
            console.log(`Hello, ${name.toUpperCase()}`);
        }
    }
}
const d = new Derived();
d.greet(); // log: Hello, world!
d.greet("reader"); // log: Hello, READER
```

子类中的方法重载需要兼容父类：

```ts
class Base {
    greet() {
        console.log("Hello, world!");
    }
}
class Derived extends Base {
    greet(name: string) {
        // err: Property 'greet' in type 'Derived' is not assignable to the same property in base type 'Base'.
        //      Type '(name: string) => void' is not assignable to type '() => void'.ts(2416)
        console.log(`Hello, ${name.toUpperCase()}`);
    }
}
```

#### Initialization Order

&emsp; &emsp; `js` 中类的初始化顺序有些奇怪，考虑如下实例：

```ts
class Base {
    name = "base";
    constructor() {
        console.log("My name is " + this.name);
    }
}
class Derived extends Base {
    name = "derived";
}

const d = new Derived(); // log: My name is base
```

最后输出的是父类中的 `name` ，而不是子类中的。这是因为 `js` 初始化类的顺序情况如下：

* The base class fields are initialized
* The base class constructor runs
* The derived class fields are initialized
* The derived class constructor runs

因此，基类构造函数中访问的字段是它自己的字段，因为此时子类中的字段初始化还没运行。

#### Inheriting Built-in Types

&emsp; &emsp; 对于继承内置类型，详情可参考[Inheriting Built-in Types](typescriptlang.org/docs/handbook/2/classes.html#inheriting-built-in-types)

### Member Visibility

#### Exposure of protected members

&emsp; &emsp; 子类不仅可以访问父类中的 `protected` 成员，还可以修改其可见性：

```ts
class Base {
  protected m = 10;
}
class Derived extends Base {
  // No modifier, so default is 'public'
  m = 15;
}
const d = new Derived();
console.log(d.m); // OK
```

#### Cross-hierarchy protected access

&emsp; &emsp; `ts` 不可以跨层次访问 `protected` 成员：

```ts
class Base {
  protected x: number = 1;
}
class Derived1 extends Base {
  protected x: number = 5;
}
class Derived2 extends Base {
  f1(other: Derived2) {
    other.x = 10;
  }
  f2(other: Base) {
    other.x = 10;
    // err: Property 'x' is protected and only accessible through an instance of class 'Derived2'. This is //       an instance of class 'Base'.
  }
}
```

#### Cross-instance private access

&emsp; &emsp; `ts` 允许跨实例访问私有成员：

```ts
class A {
  private x = 10;

  public sameAs(other: A) {
    // No error
    return other.x === this.x;
  }
}
```

### Static Members

#### Special Static Names

&emsp; &emsp; 静态成员在被编译成 `js` 时，会作为函数的一个属性：

```ts
// ts
class S {
    static test = "S!";
}
// 编译后的js
"use strict";
var S = /** @class */ (function () {
    function S() {
    }
    S.test = "S!";
    return S;
}());
//# sourceMappingURL=hello.js.map
```

因此，静态成员名称不可以与 `Function` 的属性名同名，例如 `name` , `length` , `call` 等不可以作为静态成员名称：

```ts
class S {
    static name = "S!";
    // err: Static property 'name' conflicts with built-in property 'Function.name' of constructor function 'S'.ts(2699)
}
```

### `this`

#### Arrow Functions

&emsp; &emsp; 箭头函数可以很好解决函数中的 `this` 指向问题，但是也会带来以下问题：

```ts
class Base {
    name = "MyClass";
    getName() {
        return this.name;
    }
    getNameArrowFn = () => {
        return this.name;
    };
}

class Derived extends Base {
    constructor() {
        super();
        console.log(super.getName());
        console.log(super.getNameArrowFn());
        // err: Only public and protected methods of the base class are accessible via the 'super' keyword.ts(2340)
    }
}
```

观察 `Base` 类的编译结果可以明白报错原因：

```ts
"use strict";
var Base = /** @class */ (function () {
    function Base() {
        var _this = this;
        this.name = "MyClass";
        this.getNameArrowFn = function () {
            return _this.name;
        };
    }
    Base.prototype.getName = function () {
        return this.name;
    };
    return Base;
}());
//# sourceMappingURL=hello.js.map
```

箭头函数定义在函数内部，而不是挂载在原型链上。这意味着使用箭头函数：
* 消耗更多的内存。（每一个实例都复制了一份箭头函数）
* 不可以在基类中通过`super.fnName`的形式进行访问。

#### this parameters

&emsp; &emsp; 除了使用箭头函数，还可以通过给函数传递 `this` 参数来限制 `this` ：

```ts
class MyClass {
    name = "MyClass";
    getName(this: MyClass) {
        return this.name;
    }
}
const c = new MyClass();
c.getName();

const g = c.getName;
console.log(g()); // err: The 'this' context of type 'void' is not assignable to method's 'this' of type 'MyClass'.ts(2684)
```

> 需要注意的是由于子类中也可以通过 `super.getName` 访问基类方法，对应的 `name` 也会变成子类自己的 `name` 。

#### this Types

&emsp; &emsp; `this` 也可以作为参数类型，当 `this` 作为类型来使用时：

```ts
class Box {
    content: string = "";
    sameAs(other: this) {
        return other.content === this.content;
    }
}

class DerivedBox extends Box {
    otherContent: string = "?";
}

const base = new Box();
const derived = new DerivedBox();
const derived2 = new DerivedBox();

derived.sameAs(derived2); // works

derived.sameAs(base);
// err: Argument of type 'Box' is not assignable to parameter of type 'DerivedBox'.
//      Property 'otherContent' is missing in type 'Box' but required in type 'DerivedBox'.ts(2345)
```

`derived.sameAs` 只接受来自同一派生类的其它实例作为参数。同一派生类指的是，属性兼容 `derived` 即可（实测）。

### abstract Classes and Members

&emsp; &emsp; 抽象类用来给子类提供模版，子类对抽象类中的抽象属性和方法都必须实现，抽象类本不能被实例化（抽象类中的抽象方法也只能声明不能实现）：

```ts
abstract class Base {
    abstract attr: string = 'sdf';
    abstract getName(): string;
    printName() {
        console.log("Hello, " + this.getName());
    }
}
const b = new Base(); // err: Cannot create an instance of an abstract class.ts(2511)

class Derived extends Base {
    attr = 'base attr';
    derivedAttr: string = 'attr';
    derivedMethod() {
        return 'derived method';
    }
    getName() {
        return "world";
    }
}
const d = new Derived();
d.printName();
```

### Relationships Between Classes

&emsp; &emsp; `ts` 在对类进行类型判断时，比较的是两个类的结构。即两个类的结构如果一样会被 `ts` 认为是同一个类：

```ts
class Point1 {
    x = 0;
    y = 0;
}
class Point2 {
    x = 0;
    y = 0;
}

// OK
const p: Point1 = new Point2();
```

此外，即使没有显式继承，类之间的子类型关系也存在：

```ts
class Person {
  name: string;
  age: number;
}
class Employee {
  name: string;
  age: number;
  salary: number;
}

// OK
const p: Person = new Employee();
```

类还有一些更奇怪的特性：空类没有成员。在结构类型系统中，没有成员的类型通常是其他任何类型的超类型。所以如果你写一个空类（不要！），任何东西都可以用来代替它（效果相当于 `any` ）：

```ts
class Empty { }
function fn(x: Empty) {
    // can't do anything with 'x', so I won't
}

// All OK!
fn('test');
fn(1)
fn({});
fn(fn);
```

--------

## Modules
&emsp;&emsp;[Modules](https://www.typescriptlang.org/docs/handbook/2/modules.html])