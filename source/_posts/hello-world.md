---
title: 首次记录
date: 2020-12-26 00:32:59
categories: 随笔
tags: [随笔,hello world] 
---

## Hello world!
&emsp;&emsp;早就想着利用GitHub Pages搭建个人博客，出于种种原因（主要是懒），一直被搁置。这次是因为之前的阿里云快要到期了，再加上想要通过记录来督促自己，才下定决心搭建个人博客。开始动手后，才发现住处的网络访问GitHub过于缓慢，转而改用GitLab Pages。之后就是选用Hexo搭建博客，挑了个使用人数较多的Next主题，根据官网教程配置完后，写下了第一篇博客。
