---
title: Base64、ArrayBuffer、Blob、File相关内容整理
date: 2021-01-08 23:11:32
tags: [前端, JavaScript, JS文件操作]
categories: JavaScript
---
&emsp; &emsp; 整理一下最近经常用到的几个JS操作文件相关的对象和API，文章按照[Base64](#Base64)、[ArrayBuffer](#ArrayBuffer)、[Blob](#Blob)、[File](#File)、的顺序对这几个对象进行介绍。

### Data URLs & Base64

* #### [Data URLs](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/data_URIs)

&emsp; &emsp; 说到Base64需要提到经常使用它来作为链接的 `Data URLs` 。 `Data URLs` 即前缀为 `data:` 协议的<span style="color:red">URL</span>，其允许内容创建者向文档中嵌入小文件。 `Data URLs` 由四个部分组成：前缀 `data:` 、指示数据类型的MIME类型、如果非文本则为可选的base64标记、数据本身，如下：

 ><p style="background:#eee"> `data:[<mediatype>][;base64],<data>` </p>

`mediatype` 是个MIME类型的字符串，例如 `image/jpeg` 表示JPEG图像文件。如果被省略，则默认值为 `text/plain;charset=US-ASCII` 。如果数据是文本类型，你可以直接将文本嵌入(根据文档类型，使用合适的实体字符或转义字符)。如果是二进制数据，你可以将数据进行base64编码之后再进行嵌入。实例演示如下：
{% asset_img base64.png 将Data URL分别作为img和a标签的地址 %}&emsp; &emsp; 上图中的 `img` 标签对应的图片地址为base64格式的图片（图片为本地截图转换成的base64）, `a` 标签对应的地址为base64格式的 `HTML` 文本，由于Chrome禁止打开base64格式地址的页面，添加download，将html文件下载后打开，打开如下图：
{% asset_img base64-html.png %}&emsp; &emsp; `a` 标签地址 `data:text/html,%3Ch1%3EHello%2C%20World!%3C%2Fh1%3E` 对应的 `HTML` 文本为 `<h1>Hello, World</h1>` 。说完 `Data URLs` 再回到 `Base64` 。

---------------

* #### [Base64](https://developer.mozilla.org/zh-CN/docs/Web/API/WindowBase64/Base64_encoding_and_decoding)

&emsp; &emsp; `Base64` 是一组相似的二进制到文本（binary-to-text）的编码规则，使得二进制数据在解释成 `radix-64` 的表现形式后能够用ASCII字符串的格式表示出来。在 `JavaScript` 中，有两个函数被分别用来处理解码和编码 `base64` 字符串：
&emsp; &emsp; 1. [atob()](https://developer.mozilla.org/zh-CN/docs/Web/API/WindowBase64/atob)
&emsp; &emsp; 2. [btoa()](https://developer.mozilla.org/zh-CN/docs/Web/API/WindowBase64/btoa)
&emsp; &emsp; `atob()` 函数能够解码通过base-64编码的字符串数据。相反地， `btoa()` 函数能够从二进制数据“字符串”创建一个base-64编码的ASCII字符串。
&emsp; &emsp; 每一个Base64字符实际上代表着6比特位。因此，3字节（一字节是8比特，3字节也就是24比特）的字符串/二进制文件可以转换成4个Base64字符(4x6 = 24比特)。这意味着Base64格式的字符串或文件的尺寸约是原始尺寸的133%（增加了大约33%）。代码实例如下（浏览器控制台输入，本文所示代码均在浏览器控制台中运行）：

``` javascript
const str = '123';
console.log(window.btoa(str)); // log: MTIz

const Base64Str = 'MTIz';
console.log(window.atob(Base64Str)); // log: 123
```

--------

### [ArrayBuffer](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer)

&emsp; &emsp; `ArrayBuffer` 对象用来表示通用的、固定长度的原始二进制数据缓冲区。它是一个字节数组，通常在其他语言中称为 `byte array` 。你不能直接操作 `ArrayBuffer` 的内容，而是要通过[类型数组对象](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypedArray)或 [DataView](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/DataView) 对象来操作，它们会将缓冲区中的数据表示为特定的格式，并通过这些格式来读写缓冲区的内容。 `ArrayBuffer` 相关属性与方法的代码演示如下：

``` javascript
const length = 32;
// length: ArrayBuffer的大小，单位为字节。
// 如果length大于Number.MAX_SAFE_INTEGER（>= 2 ** 53）或为负数，则抛出一个RangeError异常。
const buffer = new ArrayBuffer(length);

console.log(buffer.byteLength); // log: 32
// byteLength: 只读属性，表示ArrayBuffer的byte的大小，在ArrayBuffer构造完成时生成
```

&emsp; &emsp; `ArrayBuffer` 方法如下：
&emsp; &emsp; 1.&emsp; `ArrayBuffer.prototype.slice()` ( `ArrayBuffer.slice()` )
&emsp; &emsp; 返回一个新的 `ArrayBuffer` ，它的内容是这个 `ArrayBuffer` 的字节副本，从begin（包括），到end（不包括）。如果begin或end是负数，则指的是从数组末尾开始的索引，而不是从头开始。
&emsp; &emsp; 2.&emsp; `ArrayBuffer.isView(arg)` &nbsp; 
&emsp; &emsp; 如果参数是 `ArrayBuffer` 的视图实例则返回true，例如 `类型数组对象` 或 `DataView` 对象；否则返回 false。
&emsp; &emsp; 3.&emsp; `ArrayBuffer.transfer(oldBuffer [, newByteLength])` &nbsp; 
&emsp; &emsp; 返回一个新的 `ArrayBuffer` 对象，其内容取自 `oldBuffer` 中的数据，并且根据 `newByteLength` 的大小对数据进行截取或补0。

--------

### [Blob](https://developer.mozilla.org/zh-CN/docs/Web/API/Blob)

&emsp; &emsp; `Blob` 对象表示一个不可变、原始数据的类文件对象。它的数据可以按文本或二进制的格式进行读取，也可以转换成 [ReadableStream](https://developer.mozilla.org/zh-CN/docs/Web/API/ReadableStream) 来用于数据操作。 `Blob` 的属性和方法如下：

* #### 构造函数`Blob(blobParts[, options])`

&emsp; &emsp; 返回一个新创建的 Blob 对象，其内容由参数中给定的数组串联组成。

* #### 属性
    - `Blob.size`: `Blob` 对象中所包含数据的大小（字节）
    - `Blob.type`: 表明该 `Blob` 对象所包含数据的 `MIME` 类型。如果类型未知，则该值为空字符串。
* #### 方法
    - `Blob.slice([start[, end[, contentType]]])`
        - 返回一个新的 `Blob` 对象，包含了源 Blob 对象中指定范围内的数据。
    - `Blob.stream()`
        - 返回一个能读取blob内容的 `ReadableStream`。
    - `Blob.text()`
        - 返回一个promise且包含blob所有内容的UTF-8格式的 `USVString`.
    - `Blob.arrayBuffer()`
        - 返回一个promise且包含blob所有内容的二进制格式的 `ArrayBuffer`

> <p style="color: #e34603; background: #f9f9f9; letter-spacing: 1px; "> &emsp; slice()方法原本接受length作为第二个参数，以表示复制到新Blob对象的字节数。如果设置的参数使 start + length超出了源Blob对象的大小，则返回从开始到结尾的所有数据。

</p>

> <p style="color: #e34603; background: #f9f9f9; letter-spacing: 1px; "> &emsp; slice()方法在某些浏览器和版本上带有浏览器引擎前缀：比如Firefox 12及更早版本的blob.mozSlice()和Safari中的blob.webkitSlice()。 没有浏览器引擎前缀的老版本slice()方法有不同的语义，并且已过时。Firefox 30取消了对blob.mozSlice()的支持。</p>

&emsp; &emsp; 代码实例如下：

``` javascript
const strData = [1, 2, 3, 4, 5, 6];
const strBlob = new Blob(strData, {
    type: 'self-defined'
});

console.log(strBlob.size); // log: 6
console.log(strBlob.type); // log: self-defined
console.log(strBlob.text())
/* log:
Promise {<pending>}
__proto__: Promise
[[PromiseState]]: "fulfilled"
[[PromiseResult]]: "123456"
*/
console.log(strBlob.arrayBuffer())
/* log:
Promise {<pending>}
__proto__: Promise
[[PromiseState]]: "fulfilled"
[[PromiseResult]]: ArrayBuffer(6)
    [[Int8Array]]: Int8Array(6) [49, 50, 51, 52, 53, 54]
    [[Int16Array]]: Int16Array(3) [12849, 13363, 13877]
    [[Uint8Array]]: Uint8Array(6) [49, 50, 51, 52, 53, 54]
...
*/
```

--------

### [File](https://developer.mozilla.org/zh-CN/docs/Web/API/File)

&emsp; &emsp; `File` 接口提供有关文件的信息，并允许网页中的 `JavaScript` 访问其内容。通常情况下， `File` 对象是来自用户在&lt; input&gt; 元素上选择文件后返回的 `FileList` 对象, 也可以是来自由拖放操作生成的 `DataTransfer` 对象.
&emsp; &emsp; `File` 对象是特殊类型的 `Blob` ，且可以用在任意 `Blob` 类型的 `context` 中。比如说， `FileReader` , `URL.createObjectURL()` , `createImageBitmap()` , 及 `XMLHttpRequest.send()` 都能处理 `Blob` 和 `File` 。 `File` 的属性和方法如下：

* #### 属性
    - `File.lastModified`
        - 返回当前 `File` 对象所引用文件最后修改时间，自 `UNIX` 时间起始值（`1970年1月1日 00:00:00 UTC`）以来的毫秒数。
    - `File.lastModifiedDate`
        - 返回当前`File`对象所引用文件最后修改时间的`Date`对象。
    - `File.name`
        - 返回当前 `File` 对象所引用文件的名字。
    - `File.size`
        - 返回文件的大小
    - `File.type`
        - 返回文件的`MIME`类型
    - `File.webkitRelativePath`
        - 返回文件相关的 `path` 或 `URL`。
* #### 方法

&emsp; &emsp; `File` 接口没有定义任何方法，但是它从 `Blob` 接口继承了 `slice` 方法。
&emsp; &emsp; 代码示例如下：

``` javascript
const arr = [1, 2, 3, 4, 5, 6];
const file = new File(arr, {
    type: 'self-defined'
});

console.log(file.lastModified); // log: 1610383390916
console.log(file.lastModifiedDate);
// log: Tue Jan 12 2021 00:43:10 GMT+0800 (中国标准时间)
console.log(file.size); // log: 6
```
