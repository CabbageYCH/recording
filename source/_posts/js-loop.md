---
title: JavaScript中的for循环
date: 2021-01-05 23:47:57
tags: [JavaScript, 前端, for循环]
categories: JavaScript
---
&emsp; &emsp; 抛开最基础的 `for(let i = 0; i < len; i++){...}` 循环，本文探讨JavaScript中常用的另外三种for循环：[forEach](#forEach)、[for-in](#for-in)、以及[for-of](#for-of)

### [forEach](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach)

&emsp; &emsp; forEach特性代码展示如下：

``` javascript
const arr = ['a', '元素遍历过程中被跳过', 'b', '元素遍历过程中被跳过', 'c'];
const returnVal = arr.forEach((el, index, arrSelf) => {
    console.log(index, el, arrSelf);
    arr.shift();
})
/* 
console:
0 'a' [ 'a', '元素遍历过程中被跳过', 'b', '元素遍历过程中被跳过', 'c' ]
1 'b' [ '元素遍历过程中被跳过', 'b', '元素遍历过程中被跳过', 'c' ]
2 'c' [ 'b', '元素遍历过程中被跳过', 'c' ]
*/
console.log(returnVal); // undefined
console.log(arr); // [ '元素遍历过程中被跳过', 'c' ]
```

&emsp; &emsp; 上面代码实例中，第二次遍历时，索引1对应的元素应该是字符串 `元素遍历过程中跳过不参与循环` , 由于 `arr.shift()` ，数组第一个元素被删除，索引1对应的元素变成字符 `b` 。下文中 `for...of` 对应的代码实例同理。

> **注意:&nbsp; **<font style="color: red">除了抛出异常外，没办法跳出或是终止forEach循环，return也不行</font>

&emsp; &emsp; 除了对数组进行遍历，forEach也可以对Map与Set进行遍历:

* ##### [Set.prototype.forEach](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Set/forEach)

&emsp; &emsp; forEach方法会根据集合中元素的插入顺序，依次执行提供的回调函数。该回调函数有三个参数:
&emsp; &emsp; 1. 元素的值
&emsp; &emsp; 2. 元素的索引
&emsp; &emsp; 3. 正在遍历的集合对象
&emsp; &emsp; 但是由于集合对象中没有索引(keys)，所以前两个参数都是Set中元素的值(values)，之所以这样设计回调函数是为了和Map 以及Array的 forEach 函数用法保持一致。代码实例如下：

``` javascript
const returnVal =
    new Set(["val1", "val2", undefined]).forEach((el, index, setSelf) => {
        console.log(el, index, setSelf);
    });
/*
console:
val1 val1 Set { 'val1', 'val2', undefined }
val2 val2 Set { 'val1', 'val2', undefined }
undefined undefined Set { 'val1', 'val2', undefined }
*/
console.log(returnVal); // undefined
```

* ##### [Map.prototype.forEach](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Map/forEach)

&emsp; &emsp; forEach方法按照插入顺序依次对Map中每个键/值对执行一次提供的回调函数, 该函数的三个参数：
&emsp; &emsp; 1. 当前的 value
&emsp; &emsp; 2. 当前的 key
&emsp; &emsp; 3. 正在被遍历的 Map 对象
&emsp; &emsp; 代码实例如下：

``` javascript
const returnVal = new Map([
    ['key1', 3],
    ['key2', {}],
    ['key3', undefined]
]).forEach((el, index, mapSelf) => {
    console.log(`m[${index}] = ${el},`, mapSelf);
    mapSelf.delete('key2');
});
/*
console:
m[key1] = 3 Map { 'key1' => 3, 'key2' => {}, 'key3' => undefined }
m[key3] = undefined Map { 'key1' => 3, 'key3' => undefined }
*/
console.log(returnVal); // undefined
```

--------

### [for-in](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Statements/for...in)

&emsp; &emsp; `for...in` 语句以任意顺序遍历一个对象的除Symbol以外的可枚举属性，且 `for...in` 会遍历原型链上的属性，代码实例如下：

> <font style="color: #29d">提示：for...in不应该用于迭代一个关注索引顺序的 Array。

</font>

``` javascript
const protoProp = {
    a: 1,
    b: 2
};

const fn = function() {
    this.test = 'test';
}
fn.prototype.protoProp = protoProp;

const obj = new fn();
for (let prop in obj) {
    console.log(`obj.${prop}=${obj[prop]},`,
        `obj has ${prop}: ${obj.hasOwnProperty(prop)}`);
}
/*
console:
obj.test=test, obj has test: true
obj.protoProp=[object Object], obj has protoProp: false
*/
```

--------

### [for-of](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Statements/for...of)

&emsp; &emsp; `for...of` 语句在可迭代对象（包括Array，Map，Set，String，TypedArray，arguments 对象等等）上创建一个迭代循环，调用自定义函数进行处理，代码实例如下:

``` javascript
const arr1 = ['a', 'b', 'c'];
for (const el of arr1) {
    console.log(el, arr1);
    arr1.shift();
}
// log: a [ 'a', 'b', 'c' ]
// log: c [ 'b', 'c' ]
const map = new Map([
    ["a", 1],
    ["b", 2],
    ["c", 3]
]);
for (const el of map) {
    console.log(el);
    map.delete('b');
}
// log: [ 'a', 1 ]
// log: [ 'c', 3 ]
for (const [key, val] of map) {
    console.log(`${key}-${val}`);
}
// log: a-1
// log: c-3
```

&emsp; &emsp; 相比于 `for...in` 和 `forEach` ， `for...of` 可以在遍历中间退出，并且不会遍历原型上的属性，具体差异可看如下代码实例：

``` javascript
Array.prototype.protoProp = function() {};
const arr = [3, 5, 7];
arr.selfProp = 'self prop';

for (const i in arr) {
    console.log(i);
}
// log: 0, 1, 2, 'selfProp', 'protoProp'

console.log('---')
for (const i of arr) {
    console.log(i);
}
// log: 3, 5, 7

for (const i of arr) {
    console.log(i);
    return; // 支持遍历中间退出
}
// log: 3
```
