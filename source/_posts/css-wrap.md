---
title: css换行
date: 2020-12-31 00:32:59
tags: [CSS, 前端, 文本换行, markdown语法练习, MDN内容摘录]
categories: CSS
---

&emsp; &emsp; css中几个文本换行相关的属性一直记的有点混乱，每次都是要用到的时候再去MDN上看，索性整理出来，加深记忆(真用起来，还是得MDN...)

## 换行相关的CSS属性

&emsp; &emsp; 常用到的CSS换行属性有[overflow-wrap(word-wrap)](#overflow-wrap)、[word-break](#word-break)、[white-space](#white-space)

### [overflow-wrap](https://developer.mozilla.org/zh-CN/docs/Web/CSS/word-wrap)

> <font style="color:#f5a9a9">注：word-wrap属性原本属于微软的一个私有属性，在CSS3现在的文本规范草案中已经被重名为 overflow-wrap。word-wrap现在被当作overflow-wrap的“别名”。稳定的谷歌Chrome和Opera浏览器版本支持这种新语法

</font>

> <font style="color:#f5a9a9">与word-break相比，overflow-wrap仅在无法将整个单词放在自己的行而不会溢出的情况下才会产生中断</font>

&emsp; &emsp; **overflow-wrap**是用来说明当一个不能被分开的字符串太长而不能填充其包裹盒时，为防止其溢出，浏览器是否允许这样的单词中断换行。该属性对应的值如下：

* ***normal:*** 行只能在正常的单词断点处中断。（例如两个单词之间的空格）
* ***anywhere:*** The same as the anywhere value, with normally unbreakable words allowed to be broken at arbitrary points if there are no otherwise acceptable break points in the line, but soft wrap opportunities introduced by the word break are NOT considered when calculating min-content intrinsic sizes
* ***break-word:*** 表示如果行内没有多余的地方容纳该单词到结尾，则那些正常的不能被分割的单词会被强制分割换行

______

### [word-break](https://developer.mozilla.org/zh-CN/docs/Web/CSS/word-break)

&emsp; &emsp; **word-break**指定了怎样在单词内断行。该属性对应的值如下：

* ***normal:*** 使用默认断行规则
* ***break-all:*** 对于non-CJK（CJK指中文，日文，韩文）文本，可以在任意字符间换行
* ***keep-all:*** CJK文本不断行，non-CJK文本同normal
* ***break-word:*** 他的效果是word-break: normal 和 overflow-wrap: anywhere  的叠加

______

### [white-space](https://developer.mozilla.org/zh-CN/docs/Web/CSS/white-space)

&emsp; &emsp; **white-space**属性是用来设置如何处理元素中的空白，该属性对应的值如下：

* ***normal:*** 连续的空白符会被合并，换行符会被当作空白符来处理。换行在填充「行框盒子(line boxes)」时是必要
* ***nowrap:*** 和 normal 一样，连续的空白符会被合并。但文本内的换行无效
* ***pre:*** 连续的空白符会被保留。在遇到换行符或者&lt; br&gt; 元素时才会换行。
* ***pre-wrap:*** 连续的空白符会被保留。在遇到换行符或者&lt; br&gt; 元素，或者需要为了填充「行框盒子(line boxes)」时才会换行
* ***pre-line:*** 连续的空白符会被合并。在遇到换行符或者&lt; br&gt; 元素，或者需要为了填充「行框盒子(line boxes)」时会换行
* ***break-spaces:*** 与 pre-wrap的行为相同，除了：
    - 任何保留的空白序列总是占用空间，包括在行尾
    - 每个保留的空格字符后都存在换行机会，包括空格字符之间
    - 这样保留的空间占用空间而不会挂起，从而影响盒子的固有尺寸（最小内容大小和最大内容大小）
